//
//  Service.swift
//  ASAS
//
//  Created by apple on 5/18/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import Foundation
import  Alamofire

func alamofireGet() {
    let todoEndpoint: String = "https://jsonplaceholder.typicode.com/todos/1"
    Alamofire.request(todoEndpoint)
        .responseJSON { response in
            // check for errors
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                print("error calling GET on /todos/1")
                print(response.result.error!)
                return
            }
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: Any] else {
                print("didn't get todo object as JSON from API")
                print("Error: \(response.result.error)")
                return
            }
            // get and print the title
            guard let todoTitle = json["title"] as? String else {
                print("Could not get todo title from JSON")
                return
            }
            print("The title is: " + todoTitle)
    }
}


func makePostRequestWithToken(_ urlString: String, withParameters: [String: Any], fetchType:String, viewControl:UIViewController , completion: ((AnyObject?,Bool) -> ())?)
{
//    let todoEndpoint: String = urlString
//    let post :  String = withParameters
//    var request = URLRequest(url: URL(string: todoEndpoint)!)
//    request.setValue("application/json", forHTTPHeaderField: "Accept")
//    request.setValue("Bearer \(ModalController.getTheContentForKey("userToken") as! String)", forHTTPHeaderField: "Authorization")
//    request.httpBody = post.data(using: String.Encoding.ascii, allowLossyConversion: true)
//    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//    request.httpMethod = "POST"
//
    
    Alamofire.request(urlString, method: .post, parameters: withParameters, encoding: URLEncoding.default)
        .responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
            //    self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                do{
            
                    switch viewControl{
                        
                    case is WishlistViewController:
                        switch fetchType{
                        case "delete":
                            
                            let responss = responseDict as? AnyObject
                            let parsed = responss
                            //                        guard let status = responss?.object(forKey: "status") as? Int else{return}
                            //                        if status == 0{
                            //                            return
                            //                        }
                            let msg = responss?.object(forKey: "msg") as! String
                            viewControl.view.makeToast(msg, duration: 2.0, position: .center)
                            
                            completion!(parsed as AnyObject,true)
                            break
                            
                        case "Listing":
                            let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                            let reqJSONStr = String(data: jsonData, encoding: .utf8)
                            let data = reqJSONStr?.data(using: .utf8)
                            let parsed =  try JSONDecoder().decode([WishList].self, from: data!)
                            print(parsed[0].image)
                            completion!(parsed as AnyObject,true)
                            break
                            
                            
                        case "Addtocart":
                            let responss = responseDict as? AnyObject
                            let parsed = responss
                            //                        guard let status = responss?.object(forKey: "status") as? Int else{return}
                            //                        if status == 0{
                            //                            return
                            //                        }
                            let msg = responss?.object(forKey: "message") as! String
                            viewControl.view.makeToast(msg, duration: 2.0, position: .center)
                            
                            completion!(parsed as AnyObject,true)
                            break
                        default:
                            print("DEFAULT")
                        }
                        break
                        
                    case is CartViewController:
                        
                        switch fetchType{
                        case "delete":
                            
                            let responss = responseDict as? AnyObject
                            let parsed = responss
                            let msg = responss?.object(forKey: "msg") as! String
                            viewControl.view.makeToast(msg, duration: 2.0, position: .center)
                            completion!(parsed as AnyObject,true)
                            
                            
                            break
                        case "listing":
                            
                            
                            let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                            let reqJSONStr = String(data: jsonData, encoding: .utf8)
                            let data = reqJSONStr?.data(using: .utf8)
                            let parsed =  try JSONDecoder().decode(cartData.self, from: data!)
                            completion!(parsed as AnyObject,true)
                            
                            
                            break;
                            
                        default:
                            print("cart default")
                        }
                        
                        
                        break
                        
                        
                    default:
                        print("default")
                    }
                    
                    
                   

                }catch {
                    print("Error: \(error)")
                }
            }
    }
    
    
}



func alamofirePost(viewControl : UIViewController) {
    let todosEndpoint: String = "http://asas.us.tempcloudsite.com/api/Home/wishlistlisting"
    let newTodo: [String: Any] = ["userid": "37", "lang": "en"]
    Alamofire.request(todosEndpoint, method: .post, parameters: newTodo, encoding: URLEncoding.default)
        .responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
            //    self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                do{
                    
                    
                    switch (viewControl){
                    case is WishlistViewController:
        
//                        WishlistViewController.wishListdATA = //
                        
                        break
                        
                        
                        
                        
                    default:
                        print("iTs Defaults")
                    }
//
                    
                    
                }catch {
                    print("Error: \(error)")
                }
            }
    }
}

func alamofireDelete() {
    let firstTodoEndpoint: String = "https://jsonplaceholder.typicode.com/todos/1"
    Alamofire.request(firstTodoEndpoint, method: .delete)
        .responseJSON { response in
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                print("error calling DELETE on /todos/1")
                print(response.result.error!)
                return
            }
            print("DELETE ok")
    }
}


