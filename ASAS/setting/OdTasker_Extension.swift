//
//  ViewFunctions.swift
//  OdTasker
//
//  Created by apple on 3/1/19.
//  Copyright © 2019 aishwarya. All rights reserved.
//

import Foundation
import  UIKit
import SideMenu
import DropDown
extension UIViewController{
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func cardView(view:UIView){
        
        //     view.layer.cornerRadius = 8.0
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 6.0
        view.layer.shadowOpacity = 0.3
        
        
        
    }
    
    func setupnav(viewController : UIViewController,title: String){
        
        let toggleButton = UIButton()
        toggleButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        toggleButton.layer.cornerRadius = toggleButton.frame.width*0.5
     //   toggleButton.backgroundColor = UIColor.white
        let imageView = UIImageView()
        imageView.frame = CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0)
        imageView.center = toggleButton.center
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "hamp")
        toggleButton.addSubview(imageView)
        toggleButton.addTarget(self, action: #selector(toggleButtonClicked(_:)), for: .touchUpInside)

        let toggle = UIBarButtonItem(customView: toggleButton)
        
        let country = UIButton()
        country.frame = CGRect(x: 0, y: 0, width: 60, height: 30)
       
        let label = UILabel()
        label.tag = 10009
        label.frame = CGRect(x:0,y:0,width:40,height:20)
        label.center = country.center
        country.addSubview(label)
        
        label.text = "India"
        label.textColor = UIColor.white
        //   toggleButton.backgroundColor = UIColor.white
        let imageView4 = UIImageView()
        imageView4.frame = CGRect(x:55,y:0, width:10,height:30)
   
        imageView4.contentMode = .scaleAspectFit
       imageView4.image = UIImage(named: "down")
        country.addSubview(imageView4)
        
        let countries = UIBarButtonItem(customView: country)
        
        
        
        
        //notification buttoin
        
  
        
        
        let button = UIButton(type: .custom)
            button.frame = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0)
        let imageViews = UIImageView()
        imageViews.frame = CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0)
        imageViews.center = button.center
        imageViews.contentMode = .scaleAspectFit
        imageViews.image = UIImage(named: "pin")
        button.addSubview(imageViews)
 
        let barButtonItem = UIBarButtonItem(customView: button)
        if let viewController = viewController as? HomeViewController{
            country.addTarget(self, action: #selector(viewController.countryClicked(_:)), for: .touchUpInside)

            //self.navigationItem.leftBarButtonItems = [toggle,countries]
        }
       
        let button2 = UIButton(type: .custom)
       // button2.setImage(UIImage (named: "hamp"), for: .normal)
        button2.frame = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0)
        //button.addTarget(target, action: nil, for: .touchUpInside)
        let imageView2 = UIImageView()
        imageView2.frame = CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0)
        imageView2.center = button2.center
        imageView2.contentMode = .scaleAspectFit
        imageView2.image = UIImage(named: "cart")
        button2.addSubview(imageView2)
         button2.addTarget(self, action: #selector(viewController.cartClicked(_:)), for: .touchUpInside)
        
        let button3 = UIButton(type: .custom)
        // button2.setImage(UIImage (named: "hamp"), for: .normal)
        button3.frame = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0)
        //button.addTarget(target, action: nil, for: .touchUpInside)
        let imageView3 = UIImageView()
        imageView3.frame = CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0)
        imageView3.center = button3.center
        imageView3.contentMode = .scaleAspectFit
        imageView3.image = UIImage(named: "notify")
        button3.addSubview(imageView3)
        button3.addTarget(self, action: #selector(viewController.wishList(_:)), for: .touchUpInside)
        
        let backButton = UIButton()
        backButton.frame = CGRect(x: 0, y: 8, width: 20, height: 20)
        backButton.tag = 999
        
        
        let imageView1 = UIImageView()
        imageView1.frame = backButton.frame
        imageView1.center = backButton.center
        imageView1.contentMode = .scaleAspectFit
        imageView1.image = UIImage(named: "backArrow")
        imageView1.tintColor = UIColor.white
        backButton.addSubview(imageView1)
        
        let barButtonItem2 = UIBarButtonItem(customView: button2)
        let barButtonItem3 = UIBarButtonItem(customView: button3)
        self.navigationItem.rightBarButtonItems = [barButtonItem, barButtonItem2, barButtonItem3]
        
        
        let labels = UILabel()
        labels.frame = CGRect(x: 0, y: 0, width: 80, height: 35)
        labels.text = title
        self.navigationItem.titleView = labels
        labels.textAlignment = .left
        
        labels.textColor = UIColor.white
        
        let barBack = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(backPressed(_:)), for: .touchUpInside)
        
        if let viewController = viewController as? HomeViewController{
            self.navigationItem.leftBarButtonItems = [toggle,countries]
        }
       
        else{
            self.navigationItem.leftBarButtonItem =  barBack
        }
        
        //  toggleButton.addTarget(self, action: #selector(menuPressed(_:)), for: .touchUpInside)
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: "#333335")
    }
    
    @objc func toggleButtonClicked(_ sender : UIBarButtonItem){
        let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "leftMenuController") as! UISideMenuNavigationController
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
       SideMenuManager.default.menuWidth = UIScreen.main.bounds.size.width/2+50
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    
    @objc func backPressed(_ sender : UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func wishList(_ sender : UIBarButtonItem){
        
        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "wishList") as?  WishlistViewController
        self.navigationController?.pushViewController(viewControl! , animated: true)

        
        //        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "profile") as?  ProfileViewController
        //
        //
        //        self.navigationController?.pushViewController(viewControl! , animated: true)
        
    }
    @objc func cartClicked(_ sender : UIBarButtonItem){
        
        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cart") as?  CartViewController
        self.navigationController?.pushViewController(viewControl! , animated: true)
        
    }
    
//    @objc func countryClicked(_ sender : UIBarButtonItem){
//        let dropDown = DropDown()
//        dropDown.direction = .bottom
//        dropDown.cellHeight = 40
//        dropDown.anchorView = sender
//        dropDown.bottomOffset = CGPoint(x: 0, y:(sender.plainView.bounds.height))
//        dropDown.dataSource = ["tefg","dag","dac"]
//        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
//            if let value = sender.plainView.viewWithTag(10009) as? UILabel {
//                value.text = item
//            }
//            //  self.sate.setTitle("Select State", for: .normal)
//            print("Selected item: \(item) at index: \(index)")
//        }
//        dropDown.show()
//    }
    
     func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
}
extension UIView{
    public func cardView(){
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 6.0
        self.layer.cornerRadius = 6
        
    }
}

extension String{
    var localized: String {
        let lang = "en";//let lang = "fr";
        let path = Bundle.main.path(forResource: lang, ofType: "lproj");
        let bundle = Bundle(path: path!);
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "");
    }
}
extension NSDictionary
{
    func RemoveNullValueFromDic()-> NSDictionary
    {
        
        let mutableDictionary:NSMutableDictionary = NSMutableDictionary(dictionary: self)
        //        print("comingdict", mutableDictionary)
        for key in mutableDictionary.allKeys
        {
            if("\(mutableDictionary.object(forKey: "\(key)")!)" == "<null>" || "\(mutableDictionary.object(forKey: "\(key)")!)" is NSNull )
            {
                mutableDictionary.setValue("", forKey: key as! String)
                
                //                print("outdict", mutableDictionary)
                
            }
        }
        return mutableDictionary
        //        print("returndict", mutableDictionary)
        
    }
}


