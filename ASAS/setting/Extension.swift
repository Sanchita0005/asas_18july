//
//  Extension.swift
//  ASAS
//
//  Created by apple on 5/20/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable open class ANCustomView: UIButton {
    
    @IBInspectable var makeRound:Bool{
        set{
            let width = self.bounds.width
            let height = self.bounds.height
            if width == height && newValue{
                self.layer.masksToBounds = false
                layer.cornerRadius = height / 2
            }
        }
        get{
            return (self.bounds.width == self.bounds.height && self.bounds.height == self.layer.cornerRadius/2 ) ? true : false
        }
        
    }
}


extension UIViewController{
    
    
    func alert(message:String, title:String = "Information", defaultActionTitle: String, completion:@escaping ((_ defaultAction:Bool)->())) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: defaultActionTitle, style: .destructive, handler: {action in
            completion(true)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            completion(false)
        }
        alertController.addAction(cancelAction)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
