//
//  OdTasker_setting.swift
//  OdTasker
//
//  Created by apple on 3/5/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import Foundation

class setting : NSObject{
    
    //constants

    static let loginUserData : String = "loginUserData";
    static let loginType : String = "loginType";
    static let BASE_URL : String = "http://asas.us.tempcloudsite.com/api/";
    static let image_url : String = "http://asas.us.tempcloudsite.com/attachments/shop_images/"
    
    
    //userdefault functions
    
    static func saveTheContent(_ content : AnyObject?, WithKey key : String )-> Void {
        let defaults = UserDefaults.standard
        defaults.set(content, forKey: key as String)
        defaults.synchronize();
    }
    static func removeTheContentForKey(_ key : NSString )-> Void {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: key as String)
    }
    
    
    static func getTheContentForKey(_ key : NSString)-> AnyObject? {
        let defaults = UserDefaults.standard
        let value = defaults.object(forKey: key as String)
        return value as AnyObject
    }
    
    
    static func checkStringIsValid(_ str : NSString)-> Bool {
        if (str .isKind(of: NSNull .classForCoder()) || str .trimmingCharacters(in: CharacterSet.whitespaces).isEmpty){
            return false
        }
        return true
    }
    

}
