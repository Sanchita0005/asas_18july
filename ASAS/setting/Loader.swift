//
//  Loader.swift
//  OdTasker
//
//  Created by apple on 3/5/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import Foundation
import MBProgressHUD

class Loader: NSObject {
    static var progressView : MBProgressHUD?
    static func startLoading(_ view : UIView){
        
        progressView = MBProgressHUD.showAdded(to: view, animated: true);
        progressView?.bezelView.color = UIColor.clear
    }
    static func stopLoading(){
        
        if self.progressView != nil {
            self.progressView!.hide(true);
        }
    }
}

