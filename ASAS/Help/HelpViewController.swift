//
//  HelpViewController.swift
//  HSE
//
//  Created by SENDAN on 17/07/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
class HelpViewController: UIViewController {

    @IBOutlet weak var textview: UITextView!
    @IBOutlet weak var subject: UITextField!
    @IBOutlet weak var ProductNam: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        getdata()
        ProductNam.addTarget(self, action: #selector(viewMoreClicked(_:)), for: .touchUpInside)
        textview.layer.borderWidth = 2
        textview.layer.borderColor = UIColor.lightGray.cgColor
        ProductNam.layer.cornerRadius =  4
        ProductNam.layer.borderWidth = 1
        ProductNam.layer.borderColor = UIColor.lightGray.cgColor
        // Do any additional setup after loading the view.
    }

    
    @objc func viewMoreClicked(_ sender : UIButton){
        var countryCodeArr = [String]()
                let count = order?.orderdetails?.count
                for i in 0..<count!{

                    let str = order?.orderdetails![i].title
                    countryCodeArr.append(str!)
                }
        
        
                let dropDown = DropDown()
                dropDown.direction = .bottom
                dropDown.cellHeight = 40
                dropDown.anchorView = sender
                dropDown.bottomOffset = CGPoint(x: 0, y:(sender.plainView.bounds.height))
                dropDown.dataSource = countryCodeArr
                dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                    sender.setTitle(item, for: .normal)
        
                    //  self.sate.setTitle("Select State", for: .normal)
                    print("Selected item: \(item) at index: \(index)")
                }
                dropDown.show()
        
        
    }
//    func fghdj(){
//
    
    func getdata(){
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        let urls = setting.BASE_URL+"Home/orderquery"
        let param = ["userid":userId,"orderid":"132","lang":"en"]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
            //self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.order =  try JSONDecoder().decode(OrderDetail.self, from: data!)
//                    self.tableView.delegate = self
//                    self.tableView.dataSource = self
//                    self.tableView.reloadData()
                    
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
    }
    
    var order : OrderDetail?
    @IBAction func submit(_ sender: Any) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


class OrderDetail : Codable{
   
    let orderdetails : [orderdetail]?
    
    
}

class orderdetail : Codable{
    
    let title : String?
    let ordervendorid : String?
}
