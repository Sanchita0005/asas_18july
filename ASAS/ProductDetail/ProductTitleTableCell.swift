//
//  ProductTitleTableCell.swift
//  ASAS
//
//  Created by apple on 4/30/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import FloatRatingView
class ProductTitleTableCell: UITableViewCell {

    @IBOutlet weak var wishStatus: UILabel!
    @IBOutlet weak var WISHiMAGE: UIImageView!
    @IBOutlet weak var wishImage: UIImageView!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var rating: FloatRatingView!
    @IBOutlet weak var brandName: UILabel!
    @IBOutlet weak var wishlist: UIButton!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var more: UIButton!
    @IBOutlet weak var offValue: UILabel!
    @IBOutlet weak var oldPrice: UILabel!
    
    @IBOutlet weak var wishlistImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
