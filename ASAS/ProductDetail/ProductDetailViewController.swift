//
//  ProductDetailViewController.swift
//  ASAS
//
//  Created by apple on 4/30/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import BBannerView
import Alamofire
class ProductDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var productDetails : productDetail?
    var bbannerView = BBannerView()
    var productIds = [String:String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        setupnav(viewController: self, title: "PRODUCT DETAIL")
        getProductData()
        tableView.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func addtocart(_ sender: Any) {
        
        
        addToCartApi()
        
        
    }
    
    @objc func wishlistClicked(_ sender : UIButton){

        let urls = setting.BASE_URL+"Home/addtowhishlist"
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
             userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        let param = ["userid":userId,"prodid":productIds["productId"]!,"vendorid":productIds["vendorId"]!,"variationid":productIds["variationid"]!]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                          let cell = self.tableView.cellForRow(at: NSIndexPath(row: 1, section: 0) as IndexPath) as! ProductTitleTableCell
                    let msg = responseDict.object(forKey: "msg") as! String
                    let status = responseDict.object(forKey: "status") as! String
                    if status == "0"{
                        cell.WISHiMAGE.image=UIImage(named: "wishlist")
                          cell.wishStatus.text = "WishList"
                    }else{
                        cell.WISHiMAGE.image = UIImage(named: "wishlist_add")
                        cell.wishStatus.text = "Added To WishList"
                    }
                    self.view.makeToast(msg, duration: 2.0, position: .center)
              

                }catch {
                    print("Error: \(error)")
                }
            }
        }
  
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ProductDetailViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 210
        case 1:
            return 165
            
        case 2:
            
            if (productDetails?.variationinfo?.count)! > 0 {
              
                let count = productDetails?.variationinfo?.count
                let height  = count! * 20
                return CGFloat(height + 40)
            }else{
               return 200
            }
        default:
            return 50
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 4{
            let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "seller") as? SellerProductViewController
            viewControl!.sellerProduct = productDetails?.productvendors
            self.navigationController?.pushViewController(viewControl! , animated: true)
            
        }
    }
}

extension ProductDetailViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "bannerView",for: indexPath) as! BannerViewCell
            cell.selectionStyle = .none
            let width_Cal = UIScreen.main.bounds.size.width
            let width_Cal1 = 1080 / width_Cal
            let width_Cal2 = 500 / width_Cal1
            bbannerView = BBannerView(frame: CGRect(x: 0,
                                                    y: 0,
                                                    width: UIScreen.main.bounds.size.width ,
                                                    height: width_Cal2+20))
            cell.contentView.addSubview(bbannerView)
            bbannerView.numberOfItems = { (bannerView: BBannerView) -> Int in
                
                return (self.productDetails?.otherImgs?.count)!
            }
            
            bbannerView.viewForItem = { (bannerView: BBannerView, index: Int) -> UIView in
                
                let str =  self.productDetails?.otherImgs?[index].mediumimgurl
                
                print(str!)
                
                
                let imageView = UIImageView(frame: bannerView.bounds)
                imageView.sd_setImage(with: URL(string: str!), placeholderImage: UIImage(named: "banner-image-placeholder"))
                //                imageView.sd_setImage(with: URL(string: ("\(str)"), placeholderImage: UIImage(named: "banner-image-placeholder")))
                imageView.contentMode = UIView.ContentMode.scaleAspectFit
                imageView.clipsToBounds = true
                return imageView
            }
            
            bbannerView.tap = { (bannerView: BBannerView, index: Int) in
                print("banner2 tap: %d", index)
                
                
            }
            bbannerView.reloadData()
            bbannerView.startAutoScroll(timeIntrval: 2)
            cell.selectionStyle  = .none
            return cell
            
        }
            
        else if indexPath.row == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "productTile",for: indexPath) as! ProductTitleTableCell
            cell.selectionStyle = .none
            cell.title.text = productDetails?.product?.title
            cell.brandName.text = productDetails?.product?.brandname
            cell.price.text = "BHD\u{20B9}\(productDetails?.product?.price ?? "")"
            let ste = "BHD\u{20B9}\(productDetails?.product?.old_price ?? "")"
            let attributedString = NSMutableAttributedString(string: (ste))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedString.length))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: hexStringToUIColor(hex: "#19BBBB"), range: NSMakeRange(0, attributedString.length))
            cell.oldPrice.attributedText = attributedString
            var offer = Float((productDetails?.product?.old_price)!)!-Float((productDetails?.product?.price)!)!
            offer = (offer/Float((productDetails?.product?.old_price)!)!)*100
            print(offer)
            cell.rating.rating = Double((productDetails?.totalproductreview?[0].numberofreviews)!)!
            let st = String(format: "%.2f",offer)
            cell.offValue.text = "(\(st)% off)"
            cell.wishlist.tag = 100
            let sttt = (productDetails?.product?.whishlistval)
            if sttt == "0"{
                let image = UIImage(named: "wishlist")
                 cell.WISHiMAGE.image = image
                cell.wishStatus.text = "WishList"

            }else{
                cell.WISHiMAGE.image = UIImage(named: "wishlist_add")
                cell.wishStatus.text = "Added To WishList"


            }
           
            cell.wishlist.addTarget(self, action: #selector(wishlistClicked(_:)), for: .touchUpInside)
            return cell
            
        }
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "productFeature",for: indexPath) as! ProductFeatureTableCell
            var offset = 0
            cell.selectionStyle = .none
            
           
            
            let count = (productDetails?.variationinfo?.count)!
            print(count)
            for i in 0..<count{
                let pro = productProperty()
                pro.propertyName.text = productDetails?.variationinfo?[i].variation_name
                pro.propertyValue.text = productDetails?.variationinfo?[i].variation_values_name
                let width = cell.container.frame.size.width
                pro.frame = CGRect(x: 0, y: offset, width: Int(width), height: 20)
                cell.container.addSubview(pro)
                offset += 20
            }
            
             return cell
        }
        else if indexPath.row == 3{
              let cell = tableView.dequeueReusableCell(withIdentifier: "productQty",for: indexPath) as! ProductQtyTableCell
            cell.increment.addTarget(self, action: #selector(incrementClicked(_:)), for: .touchUpInside)

              cell.decrement.addTarget(self, action: #selector(decrementClicked(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            
            return cell
            
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Seller",for: indexPath)
            cell.selectionStyle = .none
            return cell

        }
    }
    
    
}

extension ProductDetailViewController{

    @objc func incrementClicked(_ sender : UIButton){

        stockQuantity()
    }
    @objc func decrementClicked(_ sender : UIButton){
        
        let cell = self.tableView.cellForRow(at: NSIndexPath(row: 3, section: 0) as IndexPath) as! ProductQtyTableCell
        
        var qty = Int(cell.qtyValue.text!)
        qty = qty!-1
        
        if qty == 0 {
         return
        }else{
            
         cell.qtyValue.text = "\(qty!)"
        }
        
    }
}
