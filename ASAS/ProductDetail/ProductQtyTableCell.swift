//
//  ProductQtyTableCell.swift
//  ASAS
//
//  Created by apple on 4/30/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit

class ProductQtyTableCell: UITableViewCell {

    @IBOutlet weak var qtyValue: UILabel!
    @IBOutlet weak var increment: UIButton!
    @IBOutlet weak var decrement: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
