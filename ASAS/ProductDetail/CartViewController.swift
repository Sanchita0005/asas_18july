//
//  CartViewController.swift
//  ASAS
//
//  Created by apple on 4/30/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import Alamofire

class CartViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var line: UILabel!
    
    @IBOutlet weak var cartPage: UIView!
    @IBOutlet weak var proceed: UIButton!
    @IBOutlet weak var thirdStep: UIButton!
    @IBOutlet weak var secondStep: UIButton!
    @IBOutlet weak var firstStep: UIButton!
    
    @IBOutlet weak var checkoutPage: UIView!
    var status = 0
    var cartdata : cartData?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        getCartData()
        
        checkoutPage.isHidden = true
        proceedStatus(view: firstStep, above: line)
        proceedStatus(view: secondStep, above: line)
        proceedStatus(view: thirdStep, above: line)
        firstStep.layer.borderColor = hexStringToUIColor(hex: "#04BBBB").cgColor
        firstStep.tintColor = UIColor.red
        firstStep.setTitleColor(UIColor.black, for: .normal)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func proceedClicked(_ sender: Any) {
        
        switch status {
        case 0:
            firstStep.layer.borderColor = UIColor.clear.cgColor
            let image  = UIImage(named: "orangeCheck")
            firstStep.setImage(image, for: .normal)
            secondStep.layer.borderColor = hexStringToUIColor(hex: "#04BBBB").cgColor
            secondStep.setTitleColor(UIColor.black, for: .normal)
            cartPage.isHidden = true
            let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child") as?  SelectAddressController
            checkoutPage.isHidden = false
            // let controller = storyboard?.instantiateViewController(withIdentifier: "test") as! UIViewController
            self.addChild(viewControl!)
            // 
            checkoutPage.addSubview((viewControl?.view)!)
            viewControl?.didMove(toParent: self)
            status = 1
            return
            
        case 1:
            secondStep.layer.borderColor = UIColor.clear.cgColor
            let images  = UIImage(named: "orangeCheck")
            secondStep.setImage(images, for: .normal)
            thirdStep.layer.borderColor = hexStringToUIColor(hex: "#04BBBB").cgColor
            thirdStep.setTitleColor(UIColor.black, for: .normal)
            status = 2
            
            
            return
        default:
            print("finish")
        }
        
        
        
    }
    func proceedStatus(view:UIView,above:UIView){
        
        view.layer.cornerRadius = view.layer.frame.size.width*0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.borderWidth = 1
        view.clipsToBounds = true
        self.view.insertSubview(view, aboveSubview: above)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension CartViewController : UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 180
        default:
            return 310
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let views = UIView()
        views.backgroundColor = UIColor.clear
        return views
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        default:
            return 40
        }
    }
}

extension CartViewController : UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return (cartdata?.cart?.count)!
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cart", for: indexPath) as! CartTableViewCell
            cell.productTitle.text = cartdata?.cart?[indexPath.row].Ptitle
            cell.price.text = "\u{20B9}\((cartdata?.cart?[indexPath.row].item_per_price)!)"
            let ste = "\u{20B9}\((cartdata?.cart?[indexPath.row].old_price)!)"
            
            let attributedString = NSMutableAttributedString(string: (ste))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedString.length))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: hexStringToUIColor(hex: "#19BBBB"), range: NSMakeRange(0, attributedString.length))
            
            cell.oldPrice.attributedText = attributedString
            var offer = Float((cartdata?.cart?[indexPath.row].old_price)!)!-Float((cartdata?.cart?[indexPath.row].item_per_price)!)!
            offer = (offer/Float((cartdata?.cart?[indexPath.row].old_price)!)!)*100
            print(offer)
            let st = String(format: "%.2f",offer)
            cell.offer.text = "\(st)% off"
            
            
            let str = "http://asas.us.tempcloudsite.com/attachments/shop_images/\((cartdata?.cart?[indexPath.row].Pimage!)!)"
            print(str)
            cell.productImage.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "banner-image-placeholder"))
            
            cell.selectionStyle = .none
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "bill", for: indexPath) as! BillingTableViewCell
            
            return cell
        }
        
        
    }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
}


//http://asas.us.tempcloudsite.com/api/Home/cartlisting
extension CartViewController{
    
    
    func getCartData(){
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        let urls = setting.BASE_URL+"Home/cartlisting"
        let param = ["userid":userId,"lang":"en"]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.cartdata =  try JSONDecoder().decode(cartData.self, from: data!)
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    self.tableView.reloadData()
                    
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
        
    }
    
    
}


class cartData : Codable{
    
    let cart : [cart]?
    
}
class cart : Codable{
    
    let Pimage : String?
    let Ptitle : String?
    let item_per_price : String?
    let old_price : String?
    let Vname : String?
    
    
}
