//
//  ProductDetailModal.swift
//  ASAS
//
//  Created by apple on 4/30/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import Foundation
import Alamofire

extension ProductDetailViewController{
    
    func getProductData(){
        
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        let urls = setting.BASE_URL+"Home/product_details"
        let value = setting.getTheContentForKey("userLocation")
        let latti = (value?["latitude"]!)!
        let longi = (value?["longitude"]!)!
        let param = ["userid":userId,"longitude":longi,"latitude":latti,"lang":"en","vendorid":productIds["vendorId"]!,"productid":productIds["productId"]!,"variationid":productIds["variationid"]!]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.productDetails =  try JSONDecoder().decode(productDetail.self, from: data!)
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    self.tableView.reloadData()
                    
                }catch {
                    print("Error: \(error)")
                }
                
                
                //  self.view.makeToast("Verify Code Sent Successfully, Please Check You Mail", duration: 3.0, position: .bottom)
                
            }
        }
        
    }
    
    func addToCartApi(){
        
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        
        let cell = self.tableView.cellForRow(at: NSIndexPath(row: 3, section: 0) as IndexPath) as! ProductQtyTableCell
        
        let qty = cell.qtyValue.text
        
        let urls = setting.BASE_URL+"Home/addtocart"
        let param = ["userid":userId,"quant":"\(qty!)","vendorid":productIds["vendorId"]!,"id":productIds["productId"]!,"variationid":productIds["variationid"]!]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                    
                    
                    let msg = responseDict.object(forKey: "message") as! String
                    print(msg)
                    if msg == "In Stock!"{
                        
                        
                        cell.qtyValue.text = String(describing: qty!)
                        
                    }else{
                        self.view.makeToast(msg, duration: 2.0, position: .bottom)
                        return
                    }
                    
                }catch {
                    print("Error: \(error)")
                }
                
                
                //  self.view.makeToast("Verify Code Sent Successfully, Please Check You Mail", duration: 3.0, position: .bottom)
                
            }
        }
    }
    
    func stockQuantity(){
        
        
        let cell = self.tableView.cellForRow(at: NSIndexPath(row: 3, section: 0) as IndexPath) as! ProductQtyTableCell
        
        var qty = Int(cell.qtyValue.text!)
        qty = qty!+1
        
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        let urls = setting.BASE_URL+"Home/updquantitykeypress"
        let value = setting.getTheContentForKey("userLocation")
        let latti = (value?["latitude"]!)!
        let longi = (value?["longitude"]!)!
        let param = ["userid":userId,"qtypressInput":"\(qty!)","vendorid":productIds["vendorId"]!,"itemid":productIds["productId"]!,"variationid":productIds["variationid"]!]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                    
                    
                    let msg = responseDict.object(forKey: "message") as! String
                    print(msg)
                    if msg == "In Stock!"{
                        
                        
                        cell.qtyValue.text = String(describing: qty!)
                        
                    }else{
                        self.view.makeToast(msg, duration: 2.0, position: .bottom)
                        return
                    }
                    
                }catch {
                    print("Error: \(error)")
                }
                
                
                //  self.view.makeToast("Verify Code Sent Successfully, Please Check You Mail", duration: 3.0, position: .bottom)
                
            }
        }
        
    }
    
    
}

class  productDetail: Codable {
    let otherImgs : [imgArr]?
    let product : products?
    let variationinfo : [variant]?
    let productvendors : [productVendor]?
    let totalproductreview : [review]?
}
class review : Codable{
    let numberofreviews : String?
}
class productVendor :  Codable{
    let old_price : String?
    let price : String?
    let product_variation_id : String?
    let newprodid :  String?
    let newvid : String?
    let name : String?
    
}
class variant : Codable{
    
    let variation_name : String?
    let variation_value_id : String?
    let variation_values_name : String?
}

class  imgArr: Codable {
    let imgurl : String?
    let largeimgurl : String?
    let mediumimgurl : String?
}

class products : Codable{
    let brandname : String?
    let price : String?
    let title : String?
    let old_price : String?
    let whishlistval : String?
}

