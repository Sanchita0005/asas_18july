//
//  sellerProdctTableViewCell.swift
//  ASAS
//
//  Created by apple on 5/2/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import FloatRatingView
class sellerProdctTableViewCell: UITableViewCell {

    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var offer: UILabel!
    @IBOutlet weak var oldPrice: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var addTocart: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
