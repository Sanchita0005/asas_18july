//
//  SellerProductViewController.swift
//  ASAS
//
//  Created by apple on 5/2/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import Alamofire
class SellerProductViewController: UIViewController {

    var sellerProduct : [productVendor]?
    
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        setupnav(viewController: self, title: "SELLER PRODUCT")
          tableView.register(UINib(nibName: "sellerProdctTableViewCell", bundle: nil), forCellReuseIdentifier: "sellerProdctTableViewCell");
        
        tableView.delegate = self
        tableView.dataSource = self
      
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension SellerProductViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    
}
extension SellerProductViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let value = sellerProduct?.count else{
        return 0
        }
            return value
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sellerProdctTableViewCell",for: indexPath) as! sellerProdctTableViewCell
        cell.productTitle.text = sellerProduct?[indexPath.row].name
        cell.price.text = "BHD\u{20B9}\((sellerProduct?[indexPath.row].price!)!)"
        let ste = "BHD\u{20B9}\((sellerProduct?[indexPath.row].old_price!)!)"
        let attributedString = NSMutableAttributedString(string: (ste))
        attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: hexStringToUIColor(hex: "#19BBBB"), range: NSMakeRange(0, attributedString.length))
        cell.oldPrice.attributedText = attributedString
        var offer = Float((sellerProduct?[indexPath.row].old_price)!)!-Float((sellerProduct?[indexPath.row].price)!)!
        offer = (offer/Float((sellerProduct?[indexPath.row].old_price)!)!)*100
        print(offer)
        let st = String(format: "%.2f",offer)
        cell.offer.text = "(\(st)% off)"
        cell.addTocart.addTarget(self, action: #selector(addtocartClicked(_:)), for: .touchUpInside)
        cell.addTocart.tag = indexPath.row
        cell.selectionStyle = .none
        return cell
    }
    
  
    
    
    
    
}
extension SellerProductViewController{

    @objc func addtocartClicked(_ sender : UIButton){
        
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
   
        
        let urls = setting.BASE_URL+"Home/addtocart"
        let param = ["userid":userId,"quant":"1","vendorid":(sellerProduct?[sender.tag].newvid!)!,"id":(sellerProduct?[sender.tag].newprodid!)!,"variationid":(sellerProduct?[sender.tag].product_variation_id!)!]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                    
                    
                    let msg = responseDict.object(forKey: "message") as! String
                    print(msg)
                    self.view.makeToast(msg, duration: 2.0, position: .bottom)

                    
                }catch {
                    print("Error: \(error)")
                }
                
                
                //  self.view.makeToast("Verify Code Sent Successfully, Please Check You Mail", duration: 3.0, position: .bottom)
                
            }
        }
        
        
    }

    

}
