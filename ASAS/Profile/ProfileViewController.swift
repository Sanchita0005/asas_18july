//
//  ProfileViewController.swift
//  ASAS
//
//  Created by SENDAN on 16/07/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import Alamofire
import DropDown
class ProfileViewController: UIViewController {

    
    @IBOutlet weak var phoneNo: ACFloatingTextfield!
    @IBOutlet weak var lname: ACFloatingTextfield!
    
    @IBOutlet weak var countryCode: UIButton!
    @IBOutlet weak var email: ACFloatingTextfield!
    @IBOutlet weak var male: UIButton!
    @IBOutlet weak var female: UIButton!
    @IBOutlet weak var fname: ACFloatingTextfield!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profileimg: UIImageView!
    
     var country : [Country]?
    override func viewDidLoad() {
        super.viewDidLoad()

        setupnav(viewController: self, title: "Profile")
        getProfileData()
        getCountryCode()
        countryCode.addTarget(self, action: #selector(countryNameCliked(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    @objc func countryNameCliked(_ sender : UIButton){
        
        var countryCodeArr = [String]()
        let count = country?.count
        for i in 0..<count!{
            
            let str = country?[i].Countrycode!
            countryCodeArr.append(str!)
        }
        
        
        let dropDown = DropDown()
        dropDown.direction = .bottom
        dropDown.cellHeight = 40
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:(sender.plainView.bounds.height))
        dropDown.dataSource = countryCodeArr
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            sender.setTitle(item, for: .normal)
            
            //  self.sate.setTitle("Select State", for: .normal)
            print("Selected item: \(item) at index: \(index)")
        }
        dropDown.show()
        
    }
    @IBAction func submitClicked(_ sender: Any) {
        let urls = setting.BASE_URL+"Users/useraccountupdateapi/"
        // Loader.startLoading(self.view)
        
        //userid, firstname, lastname, email, Countrycode, phonenumber, gender, userprofileimage, profile_name
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        var gender = ""
        if male.isSelected{
            gender = "0"
        }else{
            gender = "1"
        }
        
        let param = ["userid":userId,"firstname":fname.text!,"lastname":lname.text!,"email":email.text!,"Countrycode":countryCode.currentTitle!,"phonenumber":phoneNo.text!,"userprofileimage":"","gender":gender]
        
        print(param)
        Alamofire.request(urls, method:.post, parameters: param, encoding: JSONEncoding.default).validate().responseJSON {
            response in
            //   Loader.stopLoading()
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                
                do{
                    
                    
                    
                    
                }catch{
                    
                }
            }
        }
        
    }
    
    
    
    
    
    
    func getProfileData(){
        let urls = setting.BASE_URL+"Users/getuserprofileinfo/"
       // Loader.startLoading(self.view)
        
        
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        
        let param = ["userid":userId]
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
         //   Loader.stopLoading()
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                
                do{
                    
                    if let tempDict = ((responseDict.object(forKey: "userdata") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "fname") as? String{
                        self.fname.text = tempDict
                    }
                    
                    if let tempDict = ((responseDict.object(forKey: "userdata") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "email") as? String{
                        self.email.text = tempDict
                    }
                    if let tempDict = ((responseDict.object(forKey: "userdata") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "lname") as? String{
                        self.lname.text = tempDict
                    }
                    if let tempDict = ((responseDict.object(forKey: "userdata") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "phone") as? String{
                        self.phoneNo.text = tempDict
                    }
                    if let tempDict = ((responseDict.object(forKey: "userdata") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "code") as? String{
                        self.countryCode.setTitle(tempDict, for: .normal)
                    }
                    if let tempDict = ((responseDict.object(forKey: "userdata") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "gender") as? String{
                        if tempDict == "0"{
                            self.male.isSelected = true
                            self.female.isSelected = false
                        }else{
                            self.male.isSelected = false
                            self.female.isSelected = true
                        }
                    }

                    
                }catch{
                    
                }
            }
        }
    }
    func getCountryCode(){
        
        let urls = setting.BASE_URL+"Users/getcountrycode/"
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: nil, encoding: JSONEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                
                do{
                    
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.country =  try JSONDecoder().decode([Country].self, from: data!)
                    //  self.view.makeToast("Verify Code Sent Successfully, Please Check You Mail", duration: 3.0, position: .bottom)
                    
                }catch{
                    
                }
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
