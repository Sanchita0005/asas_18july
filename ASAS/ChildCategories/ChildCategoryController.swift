//
//  ChildCategoryController.swift
//  ASAS
//
//  Created by apple on 4/4/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import Alamofire
protocol PopupSampleOneProtocol {
    func okPressed(str:String)
}

class ChildCategoryController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var parentCat = ""
    var childCats : childCat?
    var sampleOneDelegate: PopupSampleOneProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupnav(viewController: self, title: "testinhg")

       // getChildCtegory()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self

        collectionViewSpacing()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
    
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isMovingFromParent {
            var parentCatArrays = [String]()
            if let parentCatArray = ModalController.getTheContentForKey("savedParentIDs") as? [String]{
                parentCatArrays = parentCatArray
                print(parentCatArrays)
                sampleOneDelegate?.okPressed(str: parentCatArrays.last!)

                if parentCatArrays.count != 0
                {
                    parentCatArrays.remove(at: parentCatArrays.count-1)
                    ModalController.saveTheContent(parentCatArrays as AnyObject, WithKey: "savedParentIDs")
                }
           
            }
        }
    }

    
    func collectionViewSpacing(){
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            let width = UIScreen.main.bounds.width
            layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            //        layout.itemSize = CGSize(width: ((width/2) - 24), height: ((width/2) - 24))
            layout.itemSize = CGSize(width: ((width/2) - 12), height: 245)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 10
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChildCategoryController : PopupSampleOneProtocol
{
    func okPressed(str: String) {
      
       print(str)
       
        let urls = setting.BASE_URL+"Categories/childcategories"
        
        Loader.startLoading(self.view)
        let param = ["pcatid":str,"lang":"en"]
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.childCats =  try JSONDecoder().decode(childCat.self, from: data!)
                    
                    if self.childCats?.productdata?.count != 0{
                        
                        
                        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productList") as?  ProductListingController
                        viewControl!.productdata = self.childCats?.productdata
                        
                        
                        viewControl?.productImageUrl = (self.childCats?.imageprefix)!
                        self.navigationController?.pushViewController(viewControl! , animated: true)
                        
                    }else{
                        //  self.collectionView.reloadData()
                        self.collectionView.reloadData()
                    }
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
      
    }
}

extension ChildCategoryController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      parentCat = (childCats?.catdata?[indexPath.row].parent!)!
        let str = (childCats?.catdata?[indexPath.row].parent!)!
        
        var parentCatArray = [String]()
        if let parentCatArrays = ModalController.getTheContentForKey("savedParentIDs") as? [String]{
           
           
                parentCatArray = parentCatArrays
                parentCatArray.append(str)
            print(parentCatArray)
                ModalController.saveTheContent(parentCatArray as AnyObject, WithKey: "savedParentIDs")
            }
        
        else{
            parentCatArray.append(str)
            print(parentCatArray)
            ModalController.saveTheContent(parentCatArray as AnyObject, WithKey: "savedParentIDs")
        }
        
        
        
//        var strs = [String]()
//        if let value = setting.getTheContentForKey("ParentId") as? [String]{
//            print(value)
//            strs = value
//            strs.append(str)
//            setting.saveTheContent(strs as AnyObject, WithKey: "ParentId")
//
//        }
//        else{
//
//            strs.append(str)
//            setting.saveTheContent(strs as AnyObject, WithKey: "ParentId")
//        }
//        if let value = setting.getTheContentForKey("ParentId") as? [String]{
//            print(value)
//
//
//        }
        getChildCtegory(index:indexPath.row)
    }
}
extension ChildCategoryController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (childCats?.catdata?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colView", for: indexPath) as! CategotriesCollectionCell
        cell.container.layer.cornerRadius = cell.container.frame.size.width*0.5
        cell.catTitle.text = childCats?.catdata?[indexPath.row].name!
        var str =  "\((self.childCats?.imageprefix!)!)\((self.childCats?.catdata?[indexPath.row].shopcat_icon!)!)"
        print(str)
         parentCat = (childCats?.catdata?[indexPath.row].parent!)!
        var strs = [String]()
     
        cell.img.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "banner-image-placeholder"))
        cell.container.backgroundColor = hexStringToUIColor(hex: "#EBEBEB")
        cell.img.contentMode = UIView.ContentMode.scaleAspectFit
        cell.img.clipsToBounds = true
        return cell
    }
    
    
}
extension ChildCategoryController : UICollectionViewDelegateFlowLayout{
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        return CGSize(width: collectionView.frame.width/2 - 5  , height: 200)
//
//
//    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        //        let size = CGSize(width: ((screenWidth/2) - 24), height: ((screenWidth/2) - 24))
        let size = CGSize(width: ((screenWidth/2) - 12), height: 145)
        return size
    }
    
}
