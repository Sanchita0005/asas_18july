//
//  childCategoryModal.swift
//  ASAS
//
//  Created by apple on 4/5/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import Foundation
import Alamofire


extension ChildCategoryController{
    func getChildCtegory(index:Int)

    {
        let urls = setting.BASE_URL+"Categories/childcategories"
        
        Loader.startLoading(self.view)
        let param = ["pcatid":(childCats?.catdata?[index].catid!)!,"lang":"en"]
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.childCats =  try JSONDecoder().decode(childCat.self, from: data!)
                    
                    if self.childCats?.productdata?.count != 0{
                        
                        
                        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productList") as?  ProductListingController
                        viewControl!.productdata = self.childCats?.productdata
    
                         viewControl?.sampleOneDelegate = self
                        viewControl?.productImageUrl = (self.childCats?.imageprefix)!
                        self.navigationController?.pushViewController(viewControl! , animated: true)
                        
                    }else{
                     //  self.collectionView.reloadData()
                        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "childCat") as? ChildCategoryController
                        viewControl!.parentCat = self.parentCat
                      
                        
                        
                        viewControl?.childCats = self.childCats
                         viewControl?.sampleOneDelegate = self
                        self.navigationController?.pushViewController(viewControl! , animated: true)
                    }
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
    }
    
}


class childCat : Codable{
    
    let catdata : [catdata]?
    let imageprefix : String?
    let productdata : [productdata]?
    
}

class catdata: Codable {
    let catid : String?
    let name : String?
    let parent : String?
    let shopcat_icon : String?
    
}

class productdata : Codable{
    let image : String?
    let old_price : String?
    let price : String?
    let productname : String?
    let vendor_name : String?
    let vid : String?
    let prodid : String?
    let product_variation_id : String?
}
