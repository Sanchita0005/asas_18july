//
//  ForgotPasswordController.swift
//  ASAS
//
//  Created by apple on 4/2/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import Alamofire
class ForgotPasswordController: UIViewController {

    @IBOutlet weak var resetPassword: UIButton!
    @IBOutlet weak var email: ACFloatingTextfield!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func resetPassword(_ sender: Any) {
        
        
        let urls = setting.BASE_URL+"Users/forgot_password/"
        let param = ["email":email.text!]
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                
                let status = responseDict.object(forKey: "Status") as! Bool
                let message = responseDict.object(forKey: "Message") as! String
                if status{
                    
                    self.view.makeToast(message, duration: 2.0, point: CGPoint(x: self.view.frame.midX, y: self.view.frame.height-150), title: "", image: nil) { didTap in
                        
                        
                    }
                    //  self.view.makeToast("Verify Code Sent Successfully, Please Check You Mail", duration: 3.0, position: .bottom)
                    
                }
            }
        
        }
        
    }
    @IBAction func CreateAccount(_ sender: Any) {
       
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
