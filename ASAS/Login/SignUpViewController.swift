//
//  SignUpViewController.swift
//  ASAS
//
//  Created by apple on 3/29/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import Alamofire
import ACFloatingTextfield_Swift
import DropDown

class SignUpViewController: UIViewController {

    @IBOutlet weak var confirm: ACFloatingTextfield!
    @IBOutlet weak var password: ACFloatingTextfield!
    @IBOutlet weak var mobilr: ACFloatingTextfield!
    @IBOutlet weak var email: ACFloatingTextfield!
    
    @IBOutlet weak var countryCode: UIButton!
    @IBOutlet weak var firstName: ACFloatingTextfield!
    @IBOutlet weak var lastName: ACFloatingTextfield!
    @IBOutlet weak var Username: ACFloatingTextfield!
    var country : [Country]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCountryCode()
        countryCode.addTarget(self, action: #selector(countryNameCliked(_:)), for: .touchUpInside)
        countryCode.layer.borderColor = UIColor.lightGray.cgColor
        countryCode.layer.borderWidth = 0.8

        // Do any additional setup after loading the view.
    }
    
    @objc func countryNameCliked(_ sender : UIButton){
        
        var countryCodeArr = [String]()
        let count = country?.count
        for i in 0..<count!{
            
        let str = country?[i].Countrycode!
            countryCodeArr.append(str!)
        }
        
        
        let dropDown = DropDown()
        dropDown.direction = .bottom
        dropDown.cellHeight = 40
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:(sender.plainView.bounds.height))
        dropDown.dataSource = countryCodeArr
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            sender.setTitle(item, for: .normal)
            
            //  self.sate.setTitle("Select State", for: .normal)
            print("Selected item: \(item) at index: \(index)")
        }
        dropDown.show()
        
    }
    
    @IBAction func signUp(_ sender: UIButton) {
        
        
        if firstName.text! == "" || lastName.text! == "" || email.text! == "" || password.text! == "" || confirm.text! == "" || mobilr.text! == "" || countryCode.currentTitle! == ""{
            self.view.makeToast("All Fields are Mandatory", duration: 3.0, position: .bottom)
            return
        }
        else if !isValidEmail(testStr: email.text!){
            self.view.makeToast("Please enter a valid email", duration: 3.0, position: .bottom)
            return
        }
        
        let urls = setting.BASE_URL+"Users/register"
        let param = ["email":email.text!,"pass":password.text!,"fname":firstName.text!,"lname":lastName.text!,"Countrycode":countryCode.currentTitle!,"phone":mobilr.text!,"pass_repeat":confirm.text!]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                
                let status = responseDict.object(forKey: "Status") as! Bool
                let message = responseDict.object(forKey: "Message") as! String
                if status{
                    
                self.view.makeToast(message)
                        
                        
                    
                    //  self.view.makeToast("Verify Code Sent Successfully, Please Check You Mail", duration: 3.0, position: .bottom)
                    
                }else{
                    
                }
            }
        }
        
    }
    
    @IBAction func backToLogin(_ sender: Any) {
        
        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as? LoginViewController
        self.navigationController?.pushViewController(viewControl! , animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SignUpViewController{
    
    func getCountryCode(){
        
        let urls = setting.BASE_URL+"Users/getcountrycode/"
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: nil, encoding: JSONEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                
                do{
                    
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.country =  try JSONDecoder().decode([Country].self, from: data!)
                    //  self.view.makeToast("Verify Code Sent Successfully, Please Check You Mail", duration: 3.0, position: .bottom)
                    
                }catch{
                    
                }
            }
        }
        
    }
    
}
