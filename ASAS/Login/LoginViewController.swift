//
//  LoginViewController.swift
//  ASAS
//
//  Created by apple on 3/29/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import Alamofire
import ACFloatingTextfield_Swift
class LoginViewController: UIViewController {

  
    @IBOutlet weak var password: ACFloatingTextfield!
    @IBOutlet weak var userName: ACFloatingTextfield!
    @IBOutlet weak var signIn: UIButton!
    @IBOutlet weak var skip: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func skipClicked(_ sender: Any) {
        
        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeVc") as? HomeViewController
        self.navigationController?.setViewControllers([viewControl!], animated: true)
        
    }
    @IBAction func forgotPASS(_ sender: Any) {
        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "forgotPass") as? ForgotPasswordController
        self.navigationController?.pushViewController(viewControl! , animated: true)
        
        
        
    }
    @IBAction func createAccount(_ sender: Any) {
        
        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signup") as? SignUpViewController
        self.navigationController?.pushViewController(viewControl! , animated: true)
        
    }
    
    
    @IBAction func SignInClicked(_ sender: Any) {
        
        
        let urls = setting.BASE_URL+"Users/login"
        let param = ["email":userName.text!,"pass":password.text!]
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                
                let status = responseDict.object(forKey: "Status") as! Bool
                let message = responseDict.object(forKey: "Message") as! String
                
                let tempDict = ((responseDict.object(forKey: "data") as! NSArray).object(at: 0) as! NSDictionary).RemoveNullValueFromDic()
                if status{
                    
                    self.view.makeToast(message, duration: 1.0, point: CGPoint(x: self.view.frame.midX, y: self.view.frame.height-150), title: "", image: nil) { didTap in
                        
                        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeVc") as? HomeViewController
                        self.navigationController?.setViewControllers([viewControl!], animated: true)
                        
                    setting.saveTheContent(tempDict, WithKey: "LoginUserData")
                       
                    }
                    //  self.view.makeToast("Verify Code Sent Successfully, Please Check You Mail", duration: 3.0, position: .bottom)
                    
                }
            }
        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
