//
//  FilterModal.swift
//  ASAS
//
//  Created by apple on 4/10/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import Foundation
import Alamofire

extension FilterViewController {
    
    func getFilterData(){
        let urls = setting.BASE_URL+"Home/getfilterlist"
        
        Loader.startLoading(self.view)
        let param = ["longitude":"50.74401520000001","latitude":"50.64722239999992","lang":"en"]
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                
                do{
                    
                    
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.filterData =  try JSONDecoder().decode(FilterData.self, from: data!)
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    self.tableView.reloadData()
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
        
        
    }
    
}


class FilterData : Codable{
    
    let filtervendorslist : [filtervendorslist]?
    let filterlocationslist : [filterlocationslist]?
    
}

class filterlocationslist : Codable{
    let name : String?
    let image : String?
    let address : String?
    let distance : String?
    
}

class filtervendorslist: Codable {
    let address : String?
    let name : String?
}
