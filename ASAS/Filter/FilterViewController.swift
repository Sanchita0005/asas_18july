//
//  FilterViewController.swift
//  ASAS
//
//  Created by apple on 4/10/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import MXSegmentedControl
class FilterViewController: UIViewController {

    var selectedIndex = 0
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var container: UIView!
    var filterData : FilterData?
    @IBOutlet weak var segmentControl: MXSegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.tableFooterView = UIView()

        getFilterData()
        
        segmentControl.append(title: "Vendor")
            .set(title: #colorLiteral(red: 0.6745098039, green: 0.1490196078, blue: 0.1843137255, alpha: 1), for: .selected)
        segmentControl.append(title: "Location")
            .set(title: #colorLiteral(red: 0.7244648933, green: 0.1273221672, blue: 0.131504029, alpha: 1), for: .selected)
        segmentControl.append(title: "Price")
            .set(title: #colorLiteral(red: 0.7244648933, green: 0.1273221672, blue: 0.131504029, alpha: 1), for: .selected)
        segmentControl.append(title: "Category")
            .set(title: #colorLiteral(red: 0.7244648933, green: 0.1273221672, blue: 0.131504029, alpha: 1), for: .selected)
        selectedIndex = 0
        
        segmentControl.indicator.lineHeight = 2.0
        segmentControl.indicator.lineView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.2705882353, alpha: 1)
        segmentControl.addTarget(self, action: #selector(changeIndex(segmentedControl:)), for: .valueChanged)
         tableView.register(UINib(nibName: "VendorViewCell", bundle: nil), forCellReuseIdentifier: "VendorViewCell");
        tableView.register(UINib(nibName: "LocationViewCell", bundle: nil), forCellReuseIdentifier: "LocationViewCell");
          tableView.register(UINib(nibName: "PriceViewCell", bundle: nil), forCellReuseIdentifier: "PriceViewCell");
        
        // Do any additional setup after loading the view.
    }
    @IBAction func crossClicked(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    @objc func changeIndex(segmentedControl: MXSegmentedControl) {
        
        switch segmentedControl.selectedIndex {
        case 0:
            segmentedControl.indicator.lineView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.2705882353, alpha: 1)
            self.selectedIndex = 0
            tableView.reloadData()
            
        case 1:
            selectedIndex = 1
            segmentedControl.indicator.lineView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.2705882353, alpha: 1)
            tableView.reloadData()
          
        case 2:
            selectedIndex = 2
            segmentedControl.indicator.lineView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.2705882353, alpha: 1)
            tableView.reloadData()
            
        case 3:
            selectedIndex = 3
            segmentedControl.indicator.lineView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.2705882353, alpha: 1)
          // tableView.reloadData()
            
        default:
            break
        }
    }
    
    @objc func checkBoxClicked(_ sender:UIButton){
        
        if sender.isSelected != true{
            sender.isSelected = true

        }else{
            sender.isSelected = false
        }
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension FilterViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedIndex == 0{
            return 60
        }else if selectedIndex == 1{
            return 80
        }else{
               return 60
        }
     
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
}

extension FilterViewController : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedIndex == 0 {
            return (filterData?.filtervendorslist?.count)!
        }else if selectedIndex == 1{
            return (filterData?.filterlocationslist?.count)!
        }else if selectedIndex == 2{
            return 1
        }else{
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if selectedIndex == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VendorViewCell", for: indexPath) as! VendorViewCell
            cell.vendorName.text = filterData?.filtervendorslist?[indexPath.row].name!
            cell.checkClicked.addTarget(self, action: #selector(checkBoxClicked(_:)), for: .touchUpInside)

            
            cell.selectionStyle = .none
            return cell
        }
        else if selectedIndex == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LocationViewCell", for: indexPath) as! LocationViewCell
            cell.name.text = filterData?.filterlocationslist?[indexPath.row].name!
            cell.address.text = filterData?.filterlocationslist?[indexPath.row].address!
            var str = ""
            print(str)
            if let value = setting.getTheContentForKey("imagePrefix") as? String{
                print(value)
                str = value
            }
           
            let str2  = filterData?.filterlocationslist?[indexPath.row].image!
            let imageUrl = "\(str)\((str2)!)"
            print(imageUrl)
            cell.selectionStyle = .none
            cell.locationImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "banner-image-placeholder"))

            return cell
            
        }else if selectedIndex == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PriceViewCell", for: indexPath) as! PriceViewCell
            cell.slider.handleDiameter = 25
            cell.slider.selectedHandleDiameterMultiplier = 1
            cell.slider.labelPosition = .below
            cell.slider.labelPadding = 10
            cell.slider.maxLabelAccessibilityLabel = "tesrt"
            cell.slider.selectedMinimum = 40
            cell.slider.selectedMaximum = 80
            cell.selectionStyle = .none
            cell.slider.handleColor = UIColor.red
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PriceViewCell", for: indexPath) as! PriceViewCell
            return cell
        }
        
    }
    
    
    
    
}
