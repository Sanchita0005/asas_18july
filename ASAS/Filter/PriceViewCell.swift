//
//  PriceViewCell.swift
//  ASAS
//
//  Created by apple on 4/10/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import TTRangeSlider
class PriceViewCell: UITableViewCell {

    @IBOutlet weak var slider: TTRangeSlider!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
