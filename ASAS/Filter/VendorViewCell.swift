//
//  VendorViewCell.swift
//  ASAS
//
//  Created by apple on 4/10/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit

class VendorViewCell: UITableViewCell {

    @IBOutlet weak var vendorName: UILabel!
    @IBOutlet weak var checkClicked: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
