//
//  OrderTableViewCell.swift
//  ASAS
//
//  Created by SENDAN on 16/07/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {

    @IBOutlet weak var view: Tracking!
    
    @IBOutlet weak var orderNo: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var qty: UILabel!
    @IBOutlet weak var vendorName: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var paymentType: UILabel!
    @IBOutlet weak var grandTotal: UILabel!
    @IBOutlet weak var couponDis: UILabel!
    @IBOutlet weak var deliveryCharge: UILabel!
    @IBOutlet weak var lowerPrice: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    
    
    
    
    @IBOutlet weak var cancel: UIButton!
    @IBOutlet weak var writeReview: UIButton!
    @IBOutlet weak var needHelp: UIButton!
    @IBOutlet weak var lowerView: UIView!
    @IBOutlet weak var viewMore: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
