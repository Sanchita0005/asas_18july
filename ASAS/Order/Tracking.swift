//
//  Tracking.swift
//  ASAS
//
//  Created by SENDAN on 17/07/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit

class Tracking: UIView {

    var view: UIView!
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var one: UIButton!
  
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        
       
      //  view.insertSubview(one, aboveSubview: label)
        view = loadViewFromNib()
//        container.layer.borderWidth = 1
//        container.layer.borderColor = UIColor.gray.cgColor
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        //extra setup
        
        // self.makeCard(self, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        
        //extra setup
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "Tracking", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
}



