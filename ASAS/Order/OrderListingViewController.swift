//
//  OrderListingViewController.swift
//  ASAS
//
//  Created by SENDAN on 16/07/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import PopupDialog
import Alamofire
class OrderListingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var selectedTag = 5555
    var orderlist: OrderList?
    override func viewDidLoad() {
        super.viewDidLoad()

        getOrderListing()
       
          self.tableView.register(UINib(nibName:"OrderTableViewCell",bundle: nil),forCellReuseIdentifier: "OrderTableViewCell")
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OrderListingViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let value =   orderlist?.myordersdetails?.count else{
            return 0
        }
        return value
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == selectedTag{
            return 480
        }else{
            return 280
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell", for: indexPath) as! OrderTableViewCell
        
        
        
        cell.view.insertSubview(cell.view.one, aboveSubview: cell.view.label)
        cell.img.layer.cornerRadius = cell.img.layer.frame.width*0.5
        cell.img.clipsToBounds = true
        cell.img.layer.borderWidth = 2
        cell.img.layer.borderColor = UIColor.black.cgColor
        
        
        
      //  let str = "\(setting.image_url)\((self.wishListdATA?[indexPath.row].image)!)"
       //"banner-image-placeholder"))
        
       
        //"banner-image-placeholder"))
        if let value = orderlist?.myordersdetails?[indexPath.row].orderdetaildata?[0].image{
             let str = "\(setting.image_url)\(value)"
            cell.img.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "banner-image-placeholder"))
        }
        
        if let value = orderlist?.myordersdetails?[indexPath.row].orderdetaildata?[0].uniqueOrderedId{
            cell.orderNo.text = value
        }
        if let value = orderlist?.myordersdetails?[indexPath.row].orderdetaildata?[0].title{
            cell.name.text = value
        }
        if let value = orderlist?.myordersdetails?[indexPath.row].orderdetaildata?[0].item_qty{
            cell.qty.text = "Qty:\(value)"
        }
        if let value = orderlist?.myordersdetails?[indexPath.row].orderdetaildata?[0].parentvendorname?.vendor_name{
            cell.vendorName.text = value
        }
        if let value = orderlist?.myordersdetails?[indexPath.row].orderdetaildata?[0].item_price{
            cell.price.text = "BHD:\(value)"
        }
        if let value = orderlist?.myordersdetails?[indexPath.row].date_time{
            cell.date.text = "Order On:\(value)"
        }
        if let value = orderlist?.myordersdetails?[indexPath.row].grand_total{
            cell.totalPrice.text = "Order Total:\(value)"
        }
        if let value = orderlist?.myordersdetails?[indexPath.row].orderdetaildata?[0].item_price{
            cell.lowerPrice.text = "BHD:\(value)"
        }
        if let value = orderlist?.myordersdetails?[indexPath.row].orderdetaildata?[0].deliverycharge{
            cell.deliveryCharge.text = "BHD:\(value)"
        }
        if let value = orderlist?.myordersdetails?[indexPath.row].orderdetaildata?[0].totaldiscountamount{
            cell.couponDis.text = "BHD:\(value)"
        }
        if let value = orderlist?.myordersdetails?[indexPath.row].grand_total{
            cell.grandTotal.text = "BHD:\(value)"
        }
        if let value = orderlist?.myordersdetails?[indexPath.row].orderdetaildata?[0].payment_type{
            cell.paymentType.text = value
        }
        
        
        
        cell.viewMore.addTarget(self, action: #selector(viewMoreClicked(_:)), for: .touchUpInside)
         cell.needHelp.addTarget(self, action: #selector(needHelpClicked(_:)), for: .touchUpInside)
         cell.writeReview.addTarget(self, action: #selector(reviewClicked(_:)), for: .touchUpInside)
        if selectedTag == indexPath.row{
            cell.lowerView.isHidden = false
        }else{
            cell.lowerView.isHidden = true
        }
        cell.viewMore.tag = indexPath.row
        return cell
    }
    @objc func reviewClicked(_ sender: UIButton){
        //  let failVC = SuccessPopUPViewController(nibName: "SuccessPopUPViewController", bundle: nil)
        let PopupVC = setPopupVC(storyboradID:"Main",viewControllerID:"writereview") as? WriteReviewViewController
        //    PopupVC!.delegates = self
        
        
        PopupVC?.popupSize = CGSize(width: self.view.frame.width - 40, height: 390)
        
        PopupVC?.popupCorner = 5
        
        PopupVC?.popupAlign = .center
        PopupVC?.popupAnimation = .bottom
        PopupVC?.touchDismiss = true
        self.presentPopup(controller: PopupVC!, completion: nil)
    }
    @objc func needHelpClicked(_ sender: UIButton){
        //  let failVC = SuccessPopUPViewController(nibName: "SuccessPopUPViewController", bundle: nil)
       let PopupVC = setPopupVC(storyboradID:"Main",viewControllerID:"help") as? HelpViewController
    //    PopupVC!.delegates = self
        
        
        PopupVC?.popupSize = CGSize(width: self.view.frame.width - 40, height: 390)
        
        PopupVC?.popupCorner = 5
        
        PopupVC?.popupAlign = .center
        PopupVC?.popupAnimation = .bottom
        PopupVC?.touchDismiss = true
        self.presentPopup(controller: PopupVC!, completion: nil)
    }
    
        @objc func viewMoreClicked(_ sender: UIButton){
            
            
              let cell = self.tableView.cellForRow(at: NSIndexPath(row: sender.tag, section: 0) as IndexPath) as! OrderTableViewCell
            if sender.tag == selectedTag {
                
                selectedTag = 5555
                cell.lowerView.isHidden = true
            }else{
                selectedTag = sender.tag
                cell.lowerView.isHidden = false
            }
            
            
            tableView.reloadData()
    }
    
}


extension OrderListingViewController {
    
    
    
    func getOrderListing(){
        
        
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        let urls = setting.BASE_URL+"Home/myorders"
        let param = ["userid":userId,"lang":"en"]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
            //self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.orderlist =  try JSONDecoder().decode(OrderList.self, from: data!)
                                        self.tableView.delegate = self
                                        self.tableView.dataSource = self
                                        self.tableView.reloadData()
                    
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
        
        
    }
    
}


class OrderList : Codable{
    let myordersdetails : [myordersdetail]?
   
    
}
class myordersdetail : Codable {
    let OrderId : String?
    let date_time : String?
    let grand_total : String?
    let orderdetaildata : [orderdetailPrice]?
}

class orderdetailPrice :  Codable{
    let deliverycharge : String?
    let finalgrandtotal :  String?
    let item_price :  String?
    let item_qty :  String?
    let orderstatusid : String?
    let parentvendorname : parentvendor?
    let payment_type : String?
    let title :  String?
    let totaldiscountamount :  String?
    let image :  String?
    let uniqueOrderedId :  String?
    
}
class parentvendor : Codable{
    
    let vendor_name :  String?
    let vendor_id :  String?
    
}
