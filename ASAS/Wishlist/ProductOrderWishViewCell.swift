//
//  ProductOrderWishViewCell.swift
//  ASAS
//
//  Created by apple on 5/17/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit

class ProductOrderWishViewCell: UITableViewCell {

    @IBOutlet weak var delete: ANCustomView!
    @IBOutlet weak var addTocart: UIButton!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var oldPrice: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var VendorName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
