//
//  WishlistViewController.swift
//  ASAS
//
//  Created by apple on 5/17/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit

class WishlistViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var wishListdATA  : [WishList]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "ProductOrderWishViewCell", bundle: nil), forCellReuseIdentifier: "ProductOrderWishViewCell");
        setupnav(viewController: self, title: "My WishList")
        
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        let todosEndpoint: String = "\(setting.BASE_URL)Home/wishlistlisting"
        let newTodo: [String: Any] = ["userid": userId, "lang": "en"]
        
        
        makePostRequestWithToken(todosEndpoint, withParameters: newTodo, fetchType: "Listing", viewControl: self) { (AnyObject, true) in
            
            
            self.wishListdATA = AnyObject as? [WishList]
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.reloadData()
            
            
        }
    }
    @objc func addTocartClicked(_ sender : UIButton){
        
        
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        let productId = self.wishListdATA?[sender.tag].prodid!
        let vendorId = self.wishListdATA?[sender.tag].vid!
        let veriationId = self.wishListdATA?[sender.tag].wish_variantes_id!
        
        let urls = setting.BASE_URL+"Home/addtocart"
        let param = ["userid":userId,"quant":"1","vendorid":vendorId!,"id":productId!,"variationid":veriationId!]
        print(param)
        makePostRequestWithToken(urls, withParameters: param, fetchType: "Addtocart", viewControl: self) { (AnyObject, true) in
            
            
            
        }
        
        
        
        
    }
    
     @objc func deleteClicked(_ sender : UIButton){
        alert(message: "Are You Sure you want to delete this Product From Wishlist", defaultActionTitle: "Delete") { (true) in
            var userId = ""
            if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
                userId = userData["id"] as! String
            }else{
                userId = (UIDevice.current.identifierForVendor?.uuidString)!
            }
            let productId = self.wishListdATA?[sender.tag].prodid!
            let vendorId = self.wishListdATA?[sender.tag].vid!
            let veriationId = self.wishListdATA?[sender.tag].wish_variantes_id!
            let todosEndpoint: String = "\(setting.BASE_URL)Home/addtowhishlist"
            let newTodo: [String: Any] = ["userid": userId, "prodid": productId!,"vendorid":vendorId!,"variationid":veriationId!]
            print(newTodo)
            makePostRequestWithToken(todosEndpoint, withParameters: newTodo, fetchType: "delete", viewControl: self, completion: { (AnyObject, true) in
                
                let url = "\(setting.BASE_URL)Home/wishlistlisting"
                let param = ["userid": userId, "lang": "en"]
                makePostRequestWithToken(url, withParameters: param, fetchType: "Listing", viewControl: self) { (AnyObject, true) in
                    
                    
                    self.wishListdATA = AnyObject as? [WishList]
                    self.tableView.reloadData()
                    
                    
                }
            })
            
            
        }
        
    }
}

extension WishlistViewController : UITableViewDelegate{

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
        
    }
}

extension WishlistViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (wishListdATA?.count)!
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductOrderWishViewCell",for: indexPath) as! ProductOrderWishViewCell
        cell.productTitle.text = self.wishListdATA?[indexPath.row].productname
        cell.price.text = "BHD\((self.wishListdATA?[indexPath.row].price)!)"
        cell.VendorName.text = "By \((self.wishListdATA?[indexPath.row].vendor_name)!)"
        let str = "\(setting.image_url)\((self.wishListdATA?[indexPath.row].image)!)"
        cell.productImage.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "banner-image-placeholder"))
        let ste = "BHD\((self.wishListdATA?[indexPath.row].old_price)!)"
        let attributedString = NSMutableAttributedString(string: (ste))
        attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: hexStringToUIColor(hex: "#19BBBB"), range: NSMakeRange(0, attributedString.length))
        cell.oldPrice.attributedText = attributedString
        
        var offer = Float((self.wishListdATA?[indexPath.row].old_price)!)!-Float((self.wishListdATA?[indexPath.row].price)!)!
        offer = (offer/Float((self.wishListdATA?[indexPath.row].old_price)!)!)*100
        print(offer)
        let st = String(format: "%.2f",offer)
        cell.discount.text = "(\(st)% off)"
        
        cell.selectionStyle = .none
        cell.delete.addTarget(self, action: #selector(deleteClicked(_:)), for: .touchUpInside)
        cell.addTocart.addTarget(self, action: #selector(addTocartClicked(_:)), for: .touchUpInside)

        return cell
    }



}
class WishList : Codable{
    
    let image : String?
    let old_price  : String?
    let productname  : String?
    let vendor_name  : String?
    let price  : String?
    let prodid : String?
    let vid : String?
    let wish_variantes_id : String?
    
}
