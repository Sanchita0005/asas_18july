//
//  AllProductListingController.swift
//  ASAS
//
//  Created by apple on 4/9/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import  Alamofire

class AllProductListingController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var productdata : Product?
    var vendorProdutc : VendorPro?
    var isFrom = false
    var vendorId = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        setupnav(viewController: self, title: "All Product")
        allproductListData()
        collectionViewSpacing()
        // Do any additional setup after loading the view.
    }
    

    func collectionViewSpacing(){
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            let width = UIScreen.main.bounds.width
            layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            //        layout.itemSize = CGSize(width: ((width/2) - 24), height: ((width/2) - 24))
            layout.itemSize = CGSize(width: ((width/2) - 12), height: 245)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 10
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class Product: Codable {
    
    let imageprefix : String?
    let products : [Productss]
    
}

class Productss: Codable {
    let image : String?
    let old_price : String?
    let price : String?
    let prodid : String?
    let productname : String?
    let url : String?
    let vendor_name : String?
    let vid : String?
    let product_variation_id : String?
}


extension AllProductListingController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productDetail") as?  ProductDetailViewController
        
        viewControl!.productIds["productId"] = productdata?.products[indexPath.row].prodid
        viewControl!.productIds["vendorId"] = productdata?.products[indexPath.row].vid
        viewControl!.productIds["variationid"] = productdata?.products[indexPath.row].product_variation_id
        self.navigationController?.pushViewController(viewControl! , animated: true)
        
        
    }
}
extension AllProductListingController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFrom{
            return (vendorProdutc?.vendorproducts?.count)!
        }else{
        return (productdata?.products.count)!
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCollectionCell", for: indexPath) as! ProductListCollectionCell
        
        if isFrom{
            cell.productNam.text = vendorProdutc?.vendorproducts?[indexPath.row].title!
            
            var str =  "\((vendorProdutc?.imageprefix)!)\((self.vendorProdutc?.vendorproducts?[indexPath.row].image!)!)"
            print(str)
            // cell.price.text = productdata?[indexPath.row].price
            let ste = "\u{20B9}\((vendorProdutc?.vendorproducts?[indexPath.row].old_price)!)"
            
            let attributedString = NSMutableAttributedString(string: (ste))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedString.length))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: hexStringToUIColor(hex: "#19BBBB"), range: NSMakeRange(0, attributedString.length))
            cell.price.text = "\u{20B9}\((vendorProdutc?.vendorproducts?[indexPath.row].price)!)"
            
            cell.oldPrice.attributedText = attributedString
            var offer = Float((vendorProdutc?.vendorproducts?[indexPath.row].old_price)!)!-Float((vendorProdutc?.vendorproducts?[indexPath.row].price)!)!
            offer = (offer/Float((vendorProdutc?.vendorproducts?[indexPath.row].old_price)!)!)*100
            print(offer)
            let st = String(format: "%.2f",offer)
            cell.offper.text = "\(st)% off"
            cell.productCat.text = vendorProdutc?.vendorproducts?[indexPath.row].Vname
            cell.productImg.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "banner-image-placeholder"))
            cell.productImg.contentMode = UIView.ContentMode.scaleAspectFit
            
            
            return cell
            
        }else{
        cell.productNam.text = productdata?.products[indexPath.row].productname!
        
        var str =  "\((productdata?.imageprefix)!)\((self.productdata?.products[indexPath.row].image!)!)"
        print(str)
        // cell.price.text = productdata?[indexPath.row].price
        let ste = "\u{20B9}\((productdata?.products[indexPath.row].old_price)!)"
        
        let attributedString = NSMutableAttributedString(string: (ste))
        attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: hexStringToUIColor(hex: "#19BBBB"), range: NSMakeRange(0, attributedString.length))
        cell.price.text = "\u{20B9}\((productdata?.products[indexPath.row].price)!)"
        
        cell.oldPrice.attributedText = attributedString
        var offer = Float((productdata?.products[indexPath.row].old_price)!)!-Float((productdata?.products[indexPath.row].price)!)!
        offer = (offer/Float((productdata?.products[indexPath.row].old_price)!)!)*100
        print(offer)
        let st = String(format: "%.2f",offer)
        cell.offper.text = "\(st)% off"
        cell.productCat.text = productdata?.products[indexPath.row].vendor_name
        cell.productImg.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "banner-image-placeholder"))
        cell.productImg.contentMode = UIView.ContentMode.scaleAspectFit
        
        
        return cell
        }
    }
    
    
}

extension AllProductListingController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        //        let size = CGSize(width: ((screenWidth/2) - 24), height: ((screenWidth/2) - 24))
        let size = CGSize(width: ((screenWidth/2) - 12), height: 230)
        return size
    }
    
}


extension AllProductListingController{
    
    func allproductListData(){
        var urls = ""
        var param = [String:String]()
        print(vendorId)
        if isFrom{
            urls = setting.BASE_URL+"/Home/getproductsofvendors"
            param = ["vid":vendorId,"lang":"en"]
        }else{
            urls = setting.BASE_URL+"Home/getallproductslisting"
            param = ["lang":"en"]
        }
            
        Loader.startLoading(self.view)
       
        
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    
                    if self.isFrom{
                        self.vendorProdutc =  try JSONDecoder().decode(VendorPro.self, from: data!)
                        self.collectionView.delegate = self
                        self.collectionView.dataSource = self
                        self.collectionView.reloadData()
                    }else{
                        self.productdata =  try JSONDecoder().decode(Product.self, from: data!)
                        self.collectionView.delegate = self
                        self.collectionView.dataSource = self
                        self.collectionView.reloadData()
                    }
                }catch {
                    print("Error: \(error)")
                }
            }
        }
        
    }
    
}


class VendorPro: Codable {
    let imageprefix : String?
    let vendorproducts : [vendorpros]?
}

class vendorpros : Codable{

    let Vname : String?
    let image : String?
    let old_price : String?
    let price : String?
    let prodid : String?
    let title : String?
    let url : String?
}
