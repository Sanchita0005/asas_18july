//
//  SideMenuViewCell.swift
//  ASAS
//
//  Created by apple on 4/9/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit

class SideMenuViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var icon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
