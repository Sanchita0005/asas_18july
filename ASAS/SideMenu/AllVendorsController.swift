//
//  AllVendorsController.swift
//  ASAS
//
//  Created by apple on 4/9/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import  Alamofire
class AllVendorsController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var vendors : vendor?
    override func viewDidLoad() {
        super.viewDidLoad()

        getallVendors()
        collectionViewSpacing()
        setupnav(viewController: self, title: "All Vendor")
    
        // Do any additional setup after loading the view.
    }
    
    func collectionViewSpacing(){
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            let width = UIScreen.main.bounds.width
            layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            //        layout.itemSize = CGSize(width: ((width/2) - 24), height: ((width/2) - 24))
            layout.itemSize = CGSize(width: ((width/2) - 12), height: 245)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 10
        }
    }
     @objc func vendorProductClicked(_ sender : UIButton){
    
        print("ProductClicekd")
     
        
        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productList") as?  ProductListingController
        
        viewControl!.vendorId = (vendors?.detail?[sender.tag].id)!
        viewControl?.isFromVendor = true
        
        
        self.navigationController?.pushViewController(viewControl! , animated: true)
        
//        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "allproductList") as? AllProductListingController
//        viewControl!.vendorId =  (vendors?.detail?[sender.tag].id!)!
//        viewControl?.isFrom = true
//        self.navigationController?.pushViewController(viewControl! , animated: true)

        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AllVendorsController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
    }
}

extension AllVendorsController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (vendors?.detail?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "vendorCell", for: indexPath) as! VendorCollectionCell
        cell.vendorName.text = vendors?.detail?[indexPath.row].name!
        var str =  "\((self.vendors?.imageurl!)!)\((self.vendors?.detail?[indexPath.row].image!)!)"
        cell.address.text = vendors?.detail?[indexPath.row].address!
        cell.vendorImage.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "banner-image-placeholder"))
        cell.vendorImage.contentMode = UIView.ContentMode.scaleAspectFit
        cell.vendorProduct.addTarget(self, action: #selector(vendorProductClicked(_:)), for: .touchUpInside)
        cell.vendorProduct.tag = indexPath.row
        cell.distance.isHidden = true
        

        return cell
    }
    
    
    
}

extension AllVendorsController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        //        let size = CGSize(width: ((screenWidth/2) - 24), height: ((screenWidth/2) - 24))
        let size = CGSize(width: ((screenWidth/2) - 12), height: 230)
        return size
        // return CGSize(width: collectionView.frame.width/2  , height: collectionView.frame.height)
        
        
    }
    
}


extension AllVendorsController{
    
    func getallVendors()
    {
        let urls = setting.BASE_URL+"Home/vendors"
        
        Loader.startLoading(self.view)
        
        Alamofire.request(urls, method:.post, parameters: nil, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.vendors =  try JSONDecoder().decode(vendor.self, from: data!)
                    self.collectionView.delegate = self
                    self.collectionView.dataSource = self
                    self.collectionView.reloadData()
                }catch {
                    print("Error: \(error)")
                }
            }
        }
    }
    
}
