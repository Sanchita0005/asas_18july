//
//  SideMenusViewController.swift
//  ASAS
//
//  Created by apple on 4/9/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import SideMenu

class SideMenusViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var imageicon = [String]()
    var names = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        

        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            let value = userData["is_contractor"] as! String

            
                if value == "1"{
                    names = ["Home", "Products","Vendor","Wishlist","Contractor","Order","Profile","Logout"]
                    imageicon = ["home","Products","Vendor","Order","Order","Order","Order","Logout"]
                }else{
                    names = ["Home", "Products","Vendor","Wishlist","Order","Profile","Logout"]
                    imageicon = ["home","Products","Vendor","Order","Order","Order","Logout"]
                }
            
            
        }else{
            names = ["Home", "Products","Vendor","Order"]
            imageicon = ["home","Products","Vendor","Order"]
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension SideMenusViewController : UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let name = names[indexPath.row]
        switch name {
        case "Home":
            dismiss(animated: true, completion: nil)

            break
           
        case "Profile":
            
            let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "profiles") as?  ProfileViewController
            self.navigationController?.pushViewController(viewControl! , animated: true)
            break
            
            
        case "Order":
            
            let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "orderList") as?  OrderListingViewController
            self.navigationController?.pushViewController(viewControl! , animated: true)
            break
        case "Wishlist":
            let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "wishList") as?  WishlistViewController
            self.navigationController?.pushViewController(viewControl! , animated: true)
            break
        case "Contractor":
            let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contractor") as? ContractorViewController
            self.navigationController?.pushViewController(viewControl! , animated: true)
            break
        case "Products":
            let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "allproductList") as? AllProductListingController
            self.navigationController?.pushViewController(viewControl! , animated: true)

        case "Vendor":
            let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "allVendors") as? AllVendorsController
            self.navigationController?.pushViewController(viewControl! , animated: true)
            break
            
        case "Logout":
           
            
              dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: Notification.Name("logoutClicked"), object: nil, userInfo: nil)
        default:
            print("bavf")
        }
        
    }
    
}
extension SideMenusViewController : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell",for: indexPath) as! SideMenuViewCell
        cell.name.text = names[indexPath.row]
        cell.icon.image = UIImage(named: imageicon[indexPath.row])
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}
