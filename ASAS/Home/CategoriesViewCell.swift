//
//  CategoriesViewCell.swift
//  ASAS
//
//  Created by apple on 4/4/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import Alamofire

class CategoriesViewCell: UITableViewCell{

    @IBOutlet weak var collectionView: UICollectionView!
   
    var category : categories?
    var parent = UIViewController()
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionViewSpacing()
        
        // Initialization code
    }

    func collectionViewSpacing(){
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            let width = UIScreen.main.bounds.width
            layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            //        layout.itemSize = CGSize(width: ((width/2) - 24), height: ((width/2) - 24))
            layout.itemSize = CGSize(width: ((width/2) - 12), height: 245)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 10
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func getChildCtegory(index:Int)
    {
        let urls = setting.BASE_URL+"Categories/childcategories"
        
        Loader.startLoading(parent.view)
        let param = ["pcatid":(category?.datatype?[index].catid)!,"lang":"en"]
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.parent.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    let childCats =  try JSONDecoder().decode(childCat.self, from: data!)
                    
                    if childCats.productdata?.count != 0{
                        
                        
                        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productList") as?  ProductListingController
                        viewControl!.productdata = childCats.productdata
                        
                        viewControl?.productImageUrl = (childCats.imageprefix)!
                        self.parent.navigationController?.pushViewController(viewControl! , animated: true)
                        
                    }else{
                        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "childCat") as? ChildCategoryController
                       // viewControl!.parentCat =
                        viewControl?.childCats = childCats
                        self.parent.navigationController?.pushViewController(viewControl! , animated: true)
                    }
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
    }

}


extension CategoriesViewCell : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       getChildCtegory(index: indexPath.row)
        
        let value = (category?.datatype?[indexPath.row].catid)!
      
        
        

    }
}

extension CategoriesViewCell : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (category?.datatype?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colView", for: indexPath) as! CategotriesCollectionCell
        cell.catTitle.text = category?.datatype?[indexPath.row].name!
        var str =  "\((self.category?.imageprefix!)!)\((self.category?.datatype?[indexPath.row].shopcat_icon!)!)"
        print(str)
        cell.img.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "banner-image-placeholder"))
        //                imageView.sd_setImage(with: URL(string: ("\(str)"), placeholderImage: UIImage(named: "banner-image-placeholder")))
        cell.container.layer.cornerRadius = cell.container.frame.size.width*0.5
        //  cell.container.alpha = 0.5
        cell.container.backgroundColor = hexStringToUIColor(hex: "#EBEBEB")
        cell.img.contentMode = UIView.ContentMode.scaleAspectFit
        cell.img.clipsToBounds = true
        return cell
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension CategoriesViewCell:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 100 , height: collectionView.frame.height)
    }
}
    

