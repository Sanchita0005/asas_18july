//
//  VendorTableViewCell.swift
//  ASAS
//
//  Created by apple on 4/4/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit

class VendorTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    var vendors : vendor?
    var parent = UIViewController()
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionViewSpacing()
        // Initialization code
    }

    func collectionViewSpacing(){
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            let width = UIScreen.main.bounds.width
            layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            //        layout.itemSize = CGSize(width: ((width/2) - 24), height: ((width/2) - 24))
            layout.itemSize = CGSize(width: ((width/2) - 12), height: 245)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 10
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension VendorTableViewCell : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let vendorId = (self.vendors?.detail?[indexPath.row].id!)!
        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productList") as?  ProductListingController
     
        viewControl!.vendorId = vendorId
        viewControl?.isFromVendor = true
        
        
        parent.navigationController?.pushViewController(viewControl! , animated: true)
        
    }
    
    
}

extension VendorTableViewCell : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (vendors?.detail?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "vendorCell", for: indexPath) as! VendorCollectionCell
        cell.vendorName.text = vendors?.detail?[indexPath.row].name!
        var str =  "\((self.vendors?.imageurl!)!)\((self.vendors?.detail?[indexPath.row].image!)!)"
        cell.address.text = vendors?.detail?[indexPath.row].address!
        let strs = (self.vendors?.imageurl!)!
        
        cell.vendorImage.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "banner-image-placeholder"))
        cell.vendorImage.contentMode = UIView.ContentMode.scaleAspectFit
        let value = self.vendors?.detail?[indexPath.row].distance!
        let distances = Double(value!)
        let st = String(format: "%.2f",distances!)
        print(st)
        cell.distance.text = String(format: "%.2f",distances!)+"Km away"
        return cell
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension VendorTableViewCell:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        //        let size = CGSize(width: ((screenWidth/2) - 24), height: ((screenWidth/2) - 24))
        let size = CGSize(width: ((screenWidth/2) - 12), height: 230)
        return size
       // return CGSize(width: collectionView.frame.width/2  , height: collectionView.frame.height)
        
        
    }
    
}


