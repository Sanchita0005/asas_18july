//
//  CategotriesCollectionCell.swift
//  ASAS
//
//  Created by apple on 4/4/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit

class CategotriesCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var catTitle: UILabel!
    
}
