//
//  HomeViewController.swift
//  ASAS
//
//  Created by apple on 4/4/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import BBannerView
import SDWebImage
import  DropDown
import  Alamofire
import CoreLocation

class HomeViewController: UIViewController,CLLocationManagerDelegate {

    @IBOutlet weak var tableView: UITableView!
    var bbannerView = BBannerView()
      var bbannerView2 = BBannerView()
    var bannerArray : BannerData?
    var category :  categories?
    var vendors : vendor?
    var country : [Country]?
    var bottomBanner : otherBanner?
    var locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupnav(viewController: self, title: "")
        getHomeData()
        registerNot()
       getCountryCode()
        determineMyCurrentLocation()
        self.navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
    }
    
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        // locationManager.allowsBackgroundLocationUpdates = true
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                if CLLocationManager.locationServicesEnabled() {
                    locationManager.startUpdatingLocation()
                    //locationManager.startUpdatingHeading()
                }
                
                var str = [String:Any]()
               // str = ["latitude":locationManager.location?.coordinate.latitude,"longitude":locationManager.location?.coordinate.longitude]
                str = ["latitude":28.6210,"longitude":77.3812]
                print(str)
                setting.saveTheContent(str as AnyObject, WithKey: "userLocation")
                let locations = locationManager.location
              //  getPlacemarkFromLocation(location: locations!)
            }
        } else {
            print("Location services are not enabled")
        }
        
        
        
      
        
       
    }
    var latitude : CLLocationDegrees?
    var longitude : CLLocationDegrees?
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
     
        
        
        
        
    }
    
    func getPlacemarkFromLocation(location: CLLocation){
        CLGeocoder().reverseGeocodeLocation(location, completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                    print(error)
                }
                if let pm = placemarks as? [CLPlacemark]{
                    
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        
                        var address = ""
                        if pm.subLocality != nil {
                            address = address + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            address = address + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            address = address + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            address = address + pm.country! + ", "
                            if let button = self.navigationItem.leftBarButtonItems{
                                if let label = button[1].customView?.viewWithTag(10009) as? UILabel{
                                    label.text = pm.country!
                                }
                            }
                        }
                        if pm.postalCode != nil {
                            address = address + pm.postalCode! + " "
                        }
                        
                        
                        //self.addressString = address
                        //self.setDriverLocation()
                        print(address)
                    }
                }
        })
    }
    func registerNot()
    {
         NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationLogout(notification:)), name: Notification.Name("logoutClicked"), object: nil)
    }
    
    @objc func countryClicked(_ sender : UIBarButtonItem){
        var countryCodeArr = [String]()
        let count = country?.count
        for i in 0..<count!{
            
            let str = country?[i].Countryname!
            countryCodeArr.append(str!)
        }
        
        
        let dropDown = DropDown()
        dropDown.direction = .bottom
        dropDown.cellHeight = 40
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:(sender.plainView.bounds.height))
        dropDown.dataSource = countryCodeArr
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
           // sender.setTitle(item, for: .normal)
            
            if let button = self.navigationItem.leftBarButtonItems{
                if let label = button[1].customView?.viewWithTag(10009) as? UILabel{
                    label.text = item
                }
            }
            //  self.sate.setTitle("Select State", for: .normal)
            print("Selected item: \(item) at index: \(index)")
        }
        dropDown.show()
    }
    func getCountryCode(){
        
        let urls = setting.BASE_URL+"Users/getcountrycode/"
      //  Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: nil, encoding: JSONEncoding.default).validate().responseJSON {
            response in
         //   Loader.stopLoading()
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                
                do{
                    
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.country =  try JSONDecoder().decode([Country].self, from: data!)
                    //  self.view.makeToast("Verify Code Sent Successfully, Please Check You Mail", duration: 3.0, position: .bottom)
                    
                }catch{
                    
                }
            }
        }
        
    }
    @objc func methodOfReceivedNotificationLogout(notification: Notification){
        
        print("alert")
      
                    let refreshAlert = UIAlertController(title: "", message: "Are you sure you want to logout of the app ?", preferredStyle: .alert)

                    refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                        print("Handle Ok logic here")

                        self.view.makeToast("Logout Successfully", duration: 1.0, point: CGPoint(x: self.view.frame.midX, y: self.view.frame.height-150), title: "", image: nil) { didTap in

                            let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as? LoginViewController
                            self.navigationController?.setViewControllers([viewControl!], animated: true)

                            setting.removeTheContentForKey("LoginUserData")

                        }
                    }))

            refreshAlert.addAction(UIAlertAction(title: "Dismiss", style: .default))

                   self.present(refreshAlert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            let width_Cal = UIScreen.main.bounds.size.width
            let width_Cal1 = 1354 / width_Cal
            let width_Cal2 = 429 / width_Cal1
            return width_Cal2 + 50
        }else if indexPath.row == 1{
            return 180
        }
        else if indexPath.row == 2 {
            if vendors?.detail?.count != 0 {
                return 250
            }else{
                return 0
            }
        }
        else{
            if bottomBanner?.otherhomebanner.count != 0 {
                let width_Cal = UIScreen.main.bounds.size.width
                let width_Cal1 = 1354 / width_Cal
                let width_Cal2 = 429 / width_Cal1
                return width_Cal2 + 50
            }else{
                return 0
            }
            
        }
        
    }
}

extension HomeViewController : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "banner",for: indexPath) as! BannerViewCell
        cell.selectionStyle = .none
        let width_Cal = UIScreen.main.bounds.size.width 
        let width_Cal1 = 1080 / width_Cal
        let width_Cal2 = 500 / width_Cal1
        bbannerView = BBannerView(frame: CGRect(x: 0,
                                                y: 0,
                                                width: UIScreen.main.bounds.size.width ,
                                                height: width_Cal2))
        cell.contentView.addSubview(bbannerView)

        bbannerView.numberOfItems = { (bannerView: BBannerView) -> Int in

            return self.bannerArray?.banner?.count ?? 0
        }

        bbannerView.viewForItem = { (bannerView: BBannerView, index: Int) -> UIView in

            var str =  "\((self.bannerArray?.imgurl!)!)\((self.bannerArray?.banner?[index].banner_image!)!)"
            
            print(str)


            let imageView = UIImageView(frame: bannerView.bounds)
            imageView.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "banner-image-placeholder"))
            //                imageView.sd_setImage(with: URL(string: ("\(str)"), placeholderImage: UIImage(named: "banner-image-placeholder")))
            imageView.contentMode = UIView.ContentMode.scaleAspectFill
            imageView.clipsToBounds = true
            return imageView
        }

        bbannerView.tap = { (bannerView: BBannerView, index: Int) in
            print("banner2 tap: %d", index)


        }
        bbannerView.reloadData()
        bbannerView.startAutoScroll(timeIntrval: 2)
            cell.selectionStyle  = .none
        return cell
        }
        
        else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "categories",for: indexPath) as! CategoriesViewCell

            cell.selectionStyle = .none
            cell.parent = self
            cell.category = category
            cell.collectionView.reloadData()
            return cell
        }
        
        else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "vendor",for: indexPath) as! VendorTableViewCell
            cell.selectionStyle = .none
            cell.vendors = vendors
            cell.parent = self
            cell.collectionView.reloadData()
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "bottomBanner",for: indexPath) as! BannerViewCell
            cell.selectionStyle = .none
            let width_Cal = UIScreen.main.bounds.size.width
            let width_Cal1 = 1080 / width_Cal
            let width_Cal2 = 500 / width_Cal1
            bbannerView2 = BBannerView(frame: CGRect(x: 10,
                                                    y: 0,
                                                    width: UIScreen.main.bounds.size.width-20,
                                                    height: width_Cal2))
            cell.contentView.addSubview(bbannerView2)
            
            bbannerView2.numberOfItems = { (bannerView: BBannerView) -> Int in
                
                return self.bottomBanner?.otherhomebanner.count ?? 0
            }
            
        
            bbannerView2.viewForItem = { (bannerView: BBannerView, index: Int) -> UIView in
                
                var str =  "\((self.bottomBanner?.imageurl!)!)\((self.bottomBanner?.otherhomebanner[index].firstslider_image!)!)"
                
                print(str)
                
                
                let imageView = UIImageView(frame: bannerView.bounds)
                imageView.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "banner-image-placeholder"))
                //                imageView.sd_setImage(with: URL(string: ("\(str)"), placeholderImage: UIImage(named: "banner-image-placeholder")))
                imageView.contentMode = UIView.ContentMode.scaleAspectFill
                imageView.clipsToBounds = true
                return imageView
            }
            
            bbannerView2.tap = { (bannerView: BBannerView, index: Int) in
                print("banner2 tap: %d", index)
                
                
            }
            bbannerView2.reloadData()
           // bbannerView2.startAutoScroll(timeIntrval: 2)
            cell.selectionStyle  = .none
            return cell

            
        }
    }
    
    
    
}
