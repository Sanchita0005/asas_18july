//
//  HomeModal.swift
//  ASAS
//
//  Created by apple on 4/4/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

extension HomeViewController{
    
    func getHomeData(){
        
        let urls = setting.BASE_URL+"Home/homebanner"
        
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: nil, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.bannerArray =  try JSONDecoder().decode(BannerData.self, from: data!)
                    print(self.bannerArray?.banner?[0].banner_image)
                    self.getCategories()
                   // self.tableView.reloadData()
                }catch {
                    print("Error: \(error)")
                }
                
               
                //  self.view.makeToast("Verify Code Sent Successfully, Please Check You Mail", duration: 3.0, position: .bottom)
                
            }
        }
    }
    
}

extension HomeViewController {
    
    func getCategories(){
        let urls = setting.BASE_URL+"Categories"
        
        Loader.startLoading(self.view)
        let param = ["lang":"en"]
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.category =  try JSONDecoder().decode(categories.self, from: data!)
                    self.getVendorList()
                }catch {
                    print("Error: \(error)")
                }
            }
        }
        
    }
    
    
    func getVendorList(){
        let urls = setting.BASE_URL+"Home/vendors"
        
        Loader.startLoading(self.view)
        let value = setting.getTheContentForKey("userLocation")
        let latti = value!["latitude"]
        let longi = value?["longitude"]
        let param = ["longitude":longi,"latitude":latti]
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.vendors =  try JSONDecoder().decode(vendor.self, from: data!)
                    let strs = self.vendors?.imageurl
                    setting.saveTheContent(strs as AnyObject, WithKey: "imagePrefix")
                    self.getotherBannerData()
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
    }
    
    
    func getotherBannerData(){
        let urls = setting.BASE_URL+"Home/getotherhomebanner"
       
        Alamofire.request(urls, method:.post, parameters: nil, encoding: URLEncoding.default).validate().responseJSON {
            response in
            
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.bottomBanner =  try JSONDecoder().decode(otherBanner.self, from: data!)
                    self.tableView.reloadData()
//                    let strs = self.vendors?.imageurl
//                    setting.saveTheContent(strs as AnyObject, WithKey: "imagePrefix")
//
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    self.tableView.reloadData()
                }catch {
                    print("Error: \(error)")
                }
            }
        }
    }
    
    
    
    
    
}

class BannerData : Codable{
    
    let banner : [banner]?
    let imgurl : String?
}

class banner: Codable {
    
    let banner_image : String?
    let banner_url : String?
}

//PARENT CATEGORIES

class categories : Codable{
    let imageprefix : String?
    let datatype : [datatype]?
}
class datatype : Codable{
    let catid : String?
    let disponsite : String?
    let name : String?
    let shopcat_icon : String?
}

//VENDOR CATEGORIES

class vendor : Codable{
    
    let detail : [detail]?
    let imageurl : String?
}
class detail: Codable {
    let address : String?
    let name : String?
    let image : String?
    let id : String?
    let distance : String?
}


class otherBanner : Codable{
    
    let imageurl : String?
    let otherhomebanner : [othBanner]
}

class othBanner : Codable{
    let firstslider_image : String?
    let firstslider_url : String?
    let firstslider_id : String?
}
