//
//  VendorCollectionCell.swift
//  ASAS
//
//  Created by apple on 4/4/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit

class VendorCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var vendorProduct: UIButton!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var vendorName: UILabel!
    @IBOutlet weak var vendorImage: UIImageView!
}
