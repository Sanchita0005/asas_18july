//
//  contractViewCell.swift
//  ASAS
//
//  Created by apple on 6/19/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit

class contractViewCell: UITableViewCell {

    @IBOutlet weak var delete: UIButton!
    @IBOutlet weak var edit: UIButton!
    @IBOutlet weak var role: UILabel!
    @IBOutlet weak var emailid: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var status: UIButton!
    @IBOutlet weak var mobileNo: UILabel!
    @IBOutlet weak var employeeCode: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
