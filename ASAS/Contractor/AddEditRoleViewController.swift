//
//  AddEditRoleViewController.swift
//  ASAS
//
//  Created by SENDAN on 15/07/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
protocol AddeditRoleDelegate {
    func dismisss()
    func okPressed(str:String,isFrom:Int)
}
class AddEditRoleViewController: UIViewController {

     var delegates: AddeditRoleDelegate?
    @IBOutlet weak var roleNmae: UITextField!
    var rolename = ""
    @IBOutlet weak var submit: UIButton!
    var isFrom = 5555
    override func viewDidLoad() {
        super.viewDidLoad()

        roleNmae.text = rolename
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeClicked(_ sender: Any) {
        delegates?.dismisss()

    }
    
    @IBAction func submit(_ sender: Any) {
        if !roleNmae.text!.isEmpty{
            delegates?.okPressed(str: roleNmae.text!,isFrom:isFrom)
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
