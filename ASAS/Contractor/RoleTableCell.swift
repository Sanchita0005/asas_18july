//
//  RoleTableCell.swift
//  ASAS
//
//  Created by SENDAN on 10/07/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit

class RoleTableCell: UITableViewCell {

    @IBOutlet weak var delete: UIButton!
    @IBOutlet weak var edit: UIButton!
    @IBOutlet weak var roleName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
