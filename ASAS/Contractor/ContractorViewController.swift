//
//  ContractorViewController.swift
//  ASAS
//
//  Created by apple on 6/19/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import Alamofire
import MXSegmentedControl
import PopupDialog
class ContractorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PopupSampleOneProtocols, AddeditRoleDelegate , AddeditProjectDelegate{
    
    
var selectedIndex = 0
    @IBOutlet weak var manageRole: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var employee : employeeList?
    var role : roleList?
    var project :Project?
    @IBOutlet weak var segmentedController: MXSegmentedControl!
    @IBOutlet weak var manageEmployee: UILabel!
    //static var sharedInstance = ContractorViewController()
    @IBOutlet weak var addrole: UIButton!
    @IBOutlet weak var viewBack: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addrole.layer.cornerRadius = addrole.frame.height / 2
        addrole.clipsToBounds = true
        setupnav(viewController: self, title: "Contractor")
        getEmployeeList()
        
        segmentedController.append(title: "Manage \nEmployee")
            .set(title: #colorLiteral(red: 0.6745098039, green: 0.1490196078, blue: 0.1843137255, alpha: 1), for: .selected)
        segmentedController.append(title: "Manage Permission")
            .set(title: #colorLiteral(red: 0.7244648933, green: 0.1273221672, blue: 0.131504029, alpha: 1), for: .selected)
        segmentedController.append(title: "Manage Project")
            .set(title: #colorLiteral(red: 0.7244648933, green: 0.1273221672, blue: 0.131504029, alpha: 1), for: .selected)
        segmentedController.append(title: "Orders")
            .set(title: #colorLiteral(red: 0.7244648933, green: 0.1273221672, blue: 0.131504029, alpha: 1), for: .selected)
        segmentedController.append(title: "Manage Roles")
            .set(title: #colorLiteral(red: 0.7244648933, green: 0.1273221672, blue: 0.131504029, alpha: 1), for: .selected)
        selectedIndex = 0
        addrole.isHidden = true
        manageRole.isHidden = true
        UILabel.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).numberOfLines = 0
        
        segmentedController.indicator.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.2705882353, alpha: 1)
        segmentedController.indicator.lineView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        segmentedController.indicator.lineHeight = 2.0
        segmentedController.indicator.lineView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.2705882353, alpha: 1)
        segmentedController.addTarget(self, action: #selector(changeIndex(segmentedControl:)), for: .valueChanged)
        viewBack.isHidden = true
        registerNot()
        self.tableView.register(UINib(nibName:"RoleTableCell",bundle: nil),forCellReuseIdentifier: "RoleTableCell")
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func addEmployee(_ sender: Any) {
       let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addemployee") as? AddEmployeeViewController
//
        viewBack.isHidden = false



        viewControl?.reloadInputViews()

        viewControl?.view.frame = CGRect(x: 40, y: 150, width: self.view.frame.width-60, height: 500)
        view.addSubview(viewControl!.view)
        addChild(viewControl!)

        viewControl!.didMove(toParent: self)
    }
    
    
    @IBAction func addRole(_ sender: Any) {
        
         //let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addEdit") as? AddEditRoleViewController
        if selectedIndex == 4{
        let PopupVC = setPopupVC(storyboradID:"Main",viewControllerID:"addEdit") as? AddEditRoleViewController
        PopupVC!.delegates = self

        
        PopupVC?.popupSize = CGSize(width: 250, height: 250)

        PopupVC?.popupCorner = 5

        PopupVC?.popupAlign = .center
        PopupVC?.popupAnimation = .bottom
        PopupVC?.touchDismiss = true
        


        self.presentPopup(controller: PopupVC!, completion: nil)
        }else{
            let PopupVC = setPopupVC(storyboradID:"Main",viewControllerID:"addEditProject") as? AddEditProjectViewController
            PopupVC!.delegates = self
            
            
            PopupVC?.popupSize = CGSize(width: 250, height: 250)
            
            PopupVC?.popupCorner = 5
            
            PopupVC?.popupAlign = .center
            PopupVC?.popupAnimation = .bottom
            PopupVC?.touchDismiss = true
            
            
            
            self.presentPopup(controller: PopupVC!, completion: nil)
        }
    }
    
    func registerNot()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationLogoutss(notification:)), name: Notification.Name("contractor"), object: nil)
    }
    @objc func methodOfReceivedNotificationLogoutss(notification: Notification){
        
        viewBack.isHidden = true
    }
    override func viewDidAppear(_ animated: Bool) {
       // getEmployeeList()
        viewBack.isHidden = true
    }
    func dismiss() {
        self.dismissPopup(completion: nil)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch selectedIndex {
        case 0:
            guard let value = employee?.data?.count else{
                    return 0
            }
            return value
        case 2:
            guard let value = project?.data?.count else{
                return 0
            }
                return value
            
            
            
        case 4:
            guard let value = role?.data?.count else{
                return 0
            }
            return value
        default:
            print("no of rows")
            return 0
        }
     
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch selectedIndex {
        case 0:
            return 300
        case 1:
            return 300
        case 2:
            return 100
        case 3:
            return 100
        case 4:
            return 100
            
        default:
            return 100
            
            print("height row")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if selectedIndex == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RoleTableCell",for: indexPath) as! RoleTableCell
            cell.delete.addTarget(self, action: #selector(deleteClicked(_:)), for: .touchUpInside)

            cell.edit.addTarget(self, action: #selector(editClicked(_:)), for: .touchUpInside)

            cell.edit.tag = indexPath.row
            cell.selectionStyle = .none
            
            cell.roleName.text = role?.data![indexPath.row].rolename
            cell.delete.tag = indexPath.row
            return cell
        }
        else if selectedIndex == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RoleTableCell",for: indexPath) as! RoleTableCell
            cell.delete.addTarget(self, action: #selector(deleteClicked(_:)), for: .touchUpInside)
            
            cell.edit.addTarget(self, action: #selector(editClick(_:)), for: .touchUpInside)
            
            cell.edit.tag = indexPath.row
            cell.selectionStyle = .none
            
            cell.roleName.text = project?.data![indexPath.row].projectname
            cell.delete.tag = indexPath.row
            return cell
        }
        
        else{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellss",for: indexPath) as! contractViewCell
        cell.emailid.text = employee?.data?[indexPath.row].email
        cell.employeeCode.text = employee?.data?[indexPath.row].password
        cell.name.text = employee?.data?[indexPath.row].fname
        cell.mobileNo.text = employee?.data?[indexPath.row].phone
        cell.role.text = employee?.data?[indexPath.row].rolename
        cell.status.layer.cornerRadius = cell.status.frame.height*0.5
        cell.edit.tag = indexPath.row
        cell.edit.addTarget(self, action: #selector(editClicked(_:)), for: .touchUpInside)
        
        
         cell.delete.addTarget(self, action: #selector(deleteClicked(_:)), for: .touchUpInside)
        cell.status.clipsToBounds = true
        cell.selectionStyle = .none
        return cell
        }
    }
    
     @objc func editClick(_ sender : UIButton){
    let PopupVC = setPopupVC(storyboradID:"Main",viewControllerID:"addEditProject") as? AddEditProjectViewController
    
    PopupVC?.isFrom = sender.tag
    PopupVC!.projectname = (project?.data?[sender.tag].projectname)!
    PopupVC?.popupSize = CGSize(width: 250, height: 250)
        PopupVC!.delegates = self
    PopupVC?.popupCorner = 5

    PopupVC?.popupAlign = .center
    PopupVC?.popupAnimation = .bottom
    PopupVC?.touchDismiss = true
    self.presentPopup(controller: PopupVC!, completion: nil)
    }
      @objc func editClicked(_ sender : UIButton){
        
        if selectedIndex == 4{
        
            let PopupVC = setPopupVC(storyboradID:"Main",viewControllerID:"addEdit") as? AddEditRoleViewController
            PopupVC!.delegates = self
            PopupVC?.isFrom = sender.tag
            PopupVC?.rolename = (role?.data![sender.tag].rolename)!
            PopupVC?.popupSize = CGSize(width: 250, height: 250)
            
            PopupVC?.popupCorner = 5
            
            PopupVC?.popupAlign = .center
            PopupVC?.popupAnimation = .bottom
            PopupVC?.touchDismiss = true
             self.presentPopup(controller: PopupVC!, completion: nil)
            
        }
        
//        if selectedIndex == 2{
//
//            let PopupVC = setPopupVC(storyboradID:"Main",viewControllerID:"addEditProject") as? AddEditProjectViewController
//            PopupVC!.delegates = self
//            PopupVC?.isFrom = sender.tag
//            PopupVC!.projectname = (project?.data?[sender.tag].projectname)!
//            PopupVC?.popupSize = CGSize(width: 250, height: 250)
//
//            PopupVC?.popupCorner = 5
//
//            PopupVC?.popupAlign = .center
//            PopupVC?.popupAnimation = .bottom
//            PopupVC?.touchDismiss = true
//            self.presentPopup(controller: PopupVC!, completion: nil)
//
//        }
        else{
            let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addemployee") as? AddEmployeeViewController
            
            viewBack.isHidden = false
            viewControl?.empId = (employee?.data?[sender.tag].id)!
            viewControl?.isFromEdit = true
            viewControl?.reloadInputViews()
            
            viewControl?.view.frame = CGRect(x: 40, y: 150, width: self.view.frame.width-60, height: 500)
            view.addSubview(viewControl!.view)
            addChild(viewControl!)
            
            viewControl!.didMove(toParent: self)
        }
        

    }
    
    @objc func deleteClicked(_ sender : UIButton){
       


        let alert = UIAlertController(title: "Are you sure you want to delete ", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        
        //        alert.addTextField(configurationHandler: { textField in
        //            textField.placeholder = "Input your name here..."
        //        })
        
        alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { action in
            switch self.selectedIndex {
            case 0:
               self.getdeleteapi(id:sender.tag)
//            case 1:
//                return 300
//            case 2:
//                return 200
//            case 3:
//                return 100
            case 4:
                 self.roledeleteapi(id:sender.tag)
                //return 100
                
            default:
              //  return 100
                
                print("height row")
            }
            
            
        }))
        
        self.present(alert, animated: true)

    }
    
    
    func getdeleteapi(id:Int){
       
        let employees = employee?.data?[id].id!
        let urls = setting.BASE_URL+"Contractor/deleteemployee"
        let param = ["employeeid":employees!]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                   
                    self.getEmployeeList()
                    
                
                    
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
    }
    
    
    @objc func changeIndex(segmentedControl: MXSegmentedControl) {
        
        switch segmentedControl.selectedIndex {
        case 0:
            segmentedControl.indicator.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.2705882353, alpha: 1)
            segmentedControl.indicator.lineView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            self.selectedIndex = 0
            manageEmployee.isHidden = false
            addrole.isHidden = true
            manageRole.isHidden = true
            tableView.reloadData()
            
        case 1:
            selectedIndex = 1
            segmentedControl.indicator.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.2705882353, alpha: 1)
            segmentedControl.indicator.lineView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            tableView.reloadData()
            
        case 2:
            selectedIndex = 2
            segmentedControl.indicator.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.2705882353, alpha: 1)
            segmentedControl.indicator.lineView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
           getProjectList()
            
            addrole.setTitle("Add Project", for: .normal)
            addrole.isHidden = false
            manageRole.isHidden = false
            manageRole.text = "Manage Project"
            manageEmployee.isHidden = true
            
        case 3:
            selectedIndex = 3
             segmentedControl.indicator.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.2705882353, alpha: 1)
            segmentedControl.indicator.lineView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
             tableView.reloadData()
        case 4:
            selectedIndex = 4
            manageRole.isHidden = false
             manageEmployee.isHidden = true
            addrole.isHidden = false
            getRoleList()
            addrole.setTitle("Add Role", for: .normal)

            segmentedControl.indicator.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.2705882353, alpha: 1)
            segmentedControl.indicator.lineView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            tableView.reloadData()
            
        default:
            break
        }
    }
    
    func closeClicked() {
        self.dismissPopup(completion: nil)

    }
    
    func SubmitPressed(str: String, isFrom: Int) {
        print("hbj")
        
        print(str)
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        var  urls = setting.BASE_URL+"Contractor/addproject"
        var param = [String:String]()
        
        if isFrom == 5555{
            param = ["userid":userId,"Project_id":"0","projectname":str]
            print(param)
        }else{
            param = ["userid":userId,"Project_id":project?.data?[isFrom].projectid!,"projectname":str] as! [String : String]
            print(param)
        }
        
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
            //self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                    
                    
                    self.dismissPopup(completion: nil)
                    self.getProjectList()
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
    }
    func okPressed(str: String,isFrom : Int) {
        print(str)
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        var  urls = setting.BASE_URL+"Contractor/addrole"
        var param = [String:String]()
        
        if isFrom == 5555{
            param = ["userid":userId,"role_id":"0","rolename":str]
        }else{
            param = ["userid":userId,"role_id":role?.data?[isFrom].roleid!,"rolename":str] as! [String : String]
            print(param)
        }
        
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
            //self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                    
                    
                    self.dismissPopup(completion: nil)
                    self.getRoleList()
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
        
    }
    
    func dismisss(){
        self.dismissPopup(completion: nil)
    }
    
    
    func getEmployeeList(){
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        let urls = setting.BASE_URL+"Contractor/allemployee"
        let param = ["userid":userId]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
                //self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.employee =  try JSONDecoder().decode(employeeList.self, from: data!)
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    self.tableView.reloadData()
                    
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


class employeeList : Codable{
    
    let data : [employeeData]?
    
}

class employeeData : Codable{
    let fname : String?
    let password : String?
    let phone : String?
    let status : String?
    let rolename : String?
    let email : String?
    let id : String?
    
}


extension ContractorViewController{
    
    
    //manage Project
    
    
    
    func getProjectList(){
        
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        let urls = setting.BASE_URL+"Contractor/getproject"
        let param = ["userid":userId]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
            //self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.project =  try JSONDecoder().decode(Project.self, from: data!)
                    //                    self.tableView.delegate = self
                    //                    self.tableView.dataSource = self
                    self.tableView.reloadData()
                    
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
    }
    
    
    
    //manage Role
    
    func getRoleList(){
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        let urls = setting.BASE_URL+"Contractor/getroles"
        let param = ["userid":userId]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
            //self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                   self.role =  try JSONDecoder().decode(roleList.self, from: data!)
//                    self.tableView.delegate = self
//                    self.tableView.dataSource = self
                   self.tableView.reloadData()
                    
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
        
    }
    
    func roledeleteapi(id:Int){
        let employees = role?.data?[id].roleid!
        let urls = setting.BASE_URL+"Contractor/deleterole"
        let param = ["roleid":employees!]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                    
                    self.getRoleList()
                    
                    
                    
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
        
    }
}



class roleList : Codable{
    let data : [datas]?
    
    
}
class datas : Codable{
    let contractorid : String?
    let role_key : String?
    let rolename : String?
    let roleid : String?
    
}

class Project : Codable{
    let data : [datass]?
   
}

class datass : Codable{
    let contractorid : String?
    let projectid : String?
    let projectname : String?
}
