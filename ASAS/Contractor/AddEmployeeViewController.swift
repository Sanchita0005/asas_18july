//
//  AddEmployeeViewController.swift
//  ASAS
//
//  Created by apple on 6/20/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import Alamofire
protocol PopupSampleOneProtocols {
    func dismiss()
   
}
class AddEmployeeViewController: UIViewController {
    @IBOutlet weak var email: UITextField!
     var sampleOneDelegate: PopupSampleOneProtocols?
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var contact: UITextField!
    @IBOutlet weak var lastname: UITextField!
    @IBOutlet weak var name: UITextField!
    var empId = ""
    //var employeeInfoDict : NSDictionary = NSDictionary()

    var isFromEdit = false
    
    
    var editData = [String:String]()
    override func viewDidLoad() {
        super.viewDidLoad()

       
        if isFromEdit{
            getemployedatabyid()
        }
       
        // Do any additional setup after loading the view.
    }
    func getemployedatabyid(){
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
       let urls = setting.BASE_URL+"Contractor/getemployeebyid"

       
        let param = ["id":empId]
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                    let tempDict = responseObject as! NSDictionary
                    
                    let employeeInfoDict = (tempDict).RemoveNullValueFromDic()
                    
                    self.name.text = (((employeeInfoDict.object(forKey: "data") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "fname") as! String)
                    self.lastname.text = (((employeeInfoDict.object(forKey: "data") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "lname") as! String)
                    self.email.text = (((employeeInfoDict.object(forKey: "data") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "email") as! String)
                    self.password.text = (((employeeInfoDict.object(forKey: "data") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "password") as! String)
                    self.confirmPassword.text = self.password.text
                    self.contact.text = (((employeeInfoDict.object(forKey: "data") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "password") as! String)
                    // ContractorViewController.sharedInstance.getEmployeeList()
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
    }
    @IBAction func closeClicke(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name("contractor"), object: nil, userInfo: nil)
        
     self.view.removeFromSuperview()
    }
    
    
    @IBAction func saveClicked(_ sender: Any) {
        var userId = ""
        if  let userData = setting.getTheContentForKey("LoginUserData") as? NSDictionary {
            userId = userData["id"] as! String
        }else{
            userId = (UIDevice.current.identifierForVendor?.uuidString)!
        }
        print(userId)
        var urls  = ""
        var param = [String:String]()
        
            if empId == ""{
                param = ["userid":userId,"firstname":name.text!,"lastname":lastname.text!,"contactnumber":contact.text!,"emailid":email.text!,"password":password.text!,"employee_id":"0","emproleid":"1"]
            }else{
                param = ["userid":userId,"firstname":name.text!,"lastname":lastname.text!,"contactnumber":contact.text!,"emailid":email.text!,"password":password.text!,"employee_id":empId,"emproleid":"1"]
            }
            urls = setting.BASE_URL+"Contractor/addemployee"
        
        print(param)
        Loader.startLoading(self.view)
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject as! NSDictionary
                do{
                 
                    let msg = responseDict.object(forKey: "msg") as! String
                    self.view.makeToast(msg, duration: 2.0, position: .center)

                   // ContractorViewController.sharedInstance.getEmployeeList()
                    
                }catch {
                    print("Error: \(error)")
                }
            }
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
