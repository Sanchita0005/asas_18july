//
//  ProductListCollectionCell.swift
//  ASAS
//
//  Created by apple on 4/5/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit

class ProductListCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productNam: UILabel!
    
    @IBOutlet weak var offper: UILabel!
    @IBOutlet weak var oldPrice: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var productCat: UILabel!
    
}
