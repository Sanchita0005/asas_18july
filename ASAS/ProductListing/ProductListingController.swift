//
//  ProductListingController.swift
//  ASAS
//
//  Created by apple on 4/5/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import UIKit
import SDWebImage

class ProductListingController: UIViewController{

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sortFilter: UIButton!
    
    var productdata : [productdata]?
    var vendorProducts : VendorProduct?
    var productImageUrl = ""
    var vendorId = ""
    var sampleOneDelegate: PopupSampleOneProtocol?

    var isFromVendor = false
    override func viewDidLoad() {
        super.viewDidLoad()

        setupnav(viewController: self, title: "category")
        
        collectionViewSpacing()
        if isFromVendor{
        productListData()
        }
        else{
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        
        sortFilter.addTarget(self, action: #selector(sortFilterClicked(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    @objc func sortFilterClicked(_ sender : UIButton){
        
         let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "filterView") as?  FilterViewController
        //let vc = self.storyboard?.instantiateViewController(withIdentifier: "filterView") as! FilterViewController
        self.present(viewControl!, animated: true, completion: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
      
        if !isFromVendor {
            if self.isMovingFromParent {
                var parentCatArrays = [String]()
                if let parentCatArray = ModalController.getTheContentForKey("savedParentIDs") as? [String]{
                    parentCatArrays = parentCatArray
                    print(parentCatArrays)
                    sampleOneDelegate?.okPressed(str: parentCatArrays.last!)
                    
                    if parentCatArrays.count != 0{
                        parentCatArrays.remove(at: parentCatArrays.count-1)
                        ModalController.saveTheContent(parentCatArrays as AnyObject, WithKey: "savedParentIDs")
                    }
                    
                }
            }
        }
        
    }
    func collectionViewSpacing(){
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            let width = UIScreen.main.bounds.width
            layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            //        layout.itemSize = CGSize(width: ((width/2) - 24), height: ((width/2) - 24))
            layout.itemSize = CGSize(width: ((width/2) - 12), height: 245)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 10
        }
    }

    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProductListingController : UICollectionViewDelegate{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.isDecelerating{
            
         sortFilter.isHidden = true
        }else{
         sortFilter.isHidden = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let viewControl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productDetail") as?  ProductDetailViewController
        if isFromVendor{
            viewControl!.productIds["productId"] = vendorProducts?.vendorproducts?[indexPath.row].id
            viewControl!.productIds["vendorId"] = vendorProducts?.vendorproducts?[indexPath.row].vendor_id
            viewControl!.productIds["variationid"] = vendorProducts?.vendorproducts?[indexPath.row].product_variation_id
        }else{
            viewControl!.productIds["productId"] = productdata?[indexPath.row].prodid
            viewControl!.productIds["vendorId"] = productdata?[indexPath.row].vid
            viewControl!.productIds["variationid"] = productdata?[indexPath.row].product_variation_id
            
        }
       

        
        self.navigationController?.pushViewController(viewControl! , animated: true)
        
        
    }
}

extension ProductListingController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFromVendor{
            return (vendorProducts?.vendorproducts?.count)!
        }else{
            return (productdata?.count)!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCollectionCell", for: indexPath) as! ProductListCollectionCell
        
        if isFromVendor{
            cell.productNam.text = vendorProducts?.vendorproducts?[indexPath.row].title
            
            var str =  "\((self.vendorProducts?.imageprefix)!)\((self.vendorProducts?.vendorproducts?[indexPath.row].image!)!)"
            print(str)
            // cell.price.text = productdata?[indexPath.row].price
            let ste = "BHD\((self.vendorProducts?.vendorproducts?[indexPath.row].old_price)!)"
            let attributedString = NSMutableAttributedString(string: (ste))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedString.length))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: hexStringToUIColor(hex: "#19BBBB"), range: NSMakeRange(0, attributedString.length))
            cell.price.text = "BHD\((self.vendorProducts?.vendorproducts?[indexPath.row].price)!)"
            var strs = Int((self.vendorProducts?.vendorproducts?[indexPath.row].old_price)!)!-Int((self.vendorProducts?.vendorproducts?[indexPath.row].price)!)!
            strs = strs/(Int((self.vendorProducts?.vendorproducts?[indexPath.row].old_price)!)!)
            strs = strs*100
            print(strs)
            var offer = Float((vendorProducts?.vendorproducts?[indexPath.row].old_price)!)!-Float((vendorProducts?.vendorproducts?[indexPath.row].price)!)!
            offer = (offer/Float((vendorProducts?.vendorproducts?[indexPath.row].old_price)!)!)*100
            print(offer)
            let st = String(format: "%.2f",offer)
            cell.offper.text = "(\(st)% off)"
            
            cell.oldPrice.attributedText = attributedString
            cell.productCat.text = "By \((self.vendorProducts?.vendorproducts?[indexPath.row].Vname)!)"
            cell.productImg.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "banner-image-placeholder"))
            cell.productImg.contentMode = UIView.ContentMode.scaleAspectFit
            return cell
        }else{
            cell.productNam.text = productdata?[indexPath.row].productname
            
            var str =  "\((self.productImageUrl))\((self.productdata?[indexPath.row].image!)!)"
            print(str)
            // cell.price.text = productdata?[indexPath.row].price
            let ste = "BHD\((productdata?[indexPath.row].old_price)!)"
            
            let attributedString = NSMutableAttributedString(string: (ste))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedString.length))
            attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: hexStringToUIColor(hex: "#19BBBB"), range: NSMakeRange(0, attributedString.length))
            cell.price.text = "BHD\((productdata?[indexPath.row].price)!)"
            
            cell.oldPrice.attributedText = attributedString
            var offer = Float((productdata?[indexPath.row].old_price)!)!-Float((productdata?[indexPath.row].price)!)!
            offer = (offer/Float((productdata?[indexPath.row].old_price)!)!)*100
            print(offer)
              let st = String(format: "%.2f",offer)
            cell.offper.text = "(\(st)% off)"
            cell.productCat.text = "By \((productdata?[indexPath.row].vendor_name)!)"
            cell.productImg.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "banner-image-placeholder"))
            cell.productImg.contentMode = UIView.ContentMode.scaleAspectFit
            return cell
        }
       
    }
 
    
}
extension ProductListingController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        //        let size = CGSize(width: ((screenWidth/2) - 24), height: ((screenWidth/2) - 24))
        let size = CGSize(width: ((screenWidth/2) - 12), height: 230)
        return size
    }
    
}
