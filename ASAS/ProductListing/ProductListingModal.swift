//
//  ProductListingModal.swift
//  ASAS
//
//  Created by apple on 4/5/19.
//  Copyright © 2019 PIIPL. All rights reserved.
//

import Foundation
import Alamofire
extension ProductListingController{

    func productListData(){
        let urls = setting.BASE_URL+"Home/getproductsofvendors"
        
        Loader.startLoading(self.view)
        let param = ["vid":vendorId,"lang":"en"]
        Alamofire.request(urls, method:.post, parameters: param, encoding: URLEncoding.default).validate().responseJSON {
            response in
            Loader.stopLoading()
            
            
            switch response.result {
            case .failure(let error):
                print(error)
                self.view.makeToast("Error Occured")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let responseDict = responseObject
                
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    self.vendorProducts =  try JSONDecoder().decode(VendorProduct.self, from: data!)
                    self.collectionView.delegate = self
                    self.collectionView.dataSource = self
                    self.collectionView.reloadData()
                }catch {
                    print("Error: \(error)")
                }
            }
        }
        
    }
    
   
    
}



class VendorProduct : Codable{
    
    let imageprefix : String?
    let vendorproducts : [vendorproducts]?
    
    
}


class vendorproducts : Codable{
    
    let image : String?
    let old_price : String?
    let price : String?
    let title : String?
    let Vname : String?
    let vid : String?
    let id : String?
    let product_variation_id : String?
    let vendor_id : String?
}
